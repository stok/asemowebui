/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.web.ui.stripes;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.exception.EmailNotConfiguredException;
import fi.stok.asemo.biz.exception.UserNotFoundException;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.biz.AsemoBizWrapper;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.validation.*;

/**
 *
 * @author Juha Loukkola
 */
public class RequestNewPasswordActionBean extends AsemoActionBean {

    private String username;
    private AsemoBizWrapper bizWrapper;
    private Business biz;

    public RequestNewPasswordActionBean() {
        this.bizWrapper = new AsemoBizWrapper();
        this.biz = this.bizWrapper.getBusinessBean();
    }

    public String getUsername() {
        return username;
    }

    @Validate(required = true, on = "send")
    public void setUsername(String userName) {
        this.username = userName;
    }

    @DefaultHandler
    public Resolution view() {
        return new ForwardResolution(Urls.USER_REQUEST_PASSWORD);
    }

    public Resolution send() {
        try {
            this.biz.processPasswordRequest(username);
        } catch (UserNotFoundException ex) {
            ValidationError error = new LocalizableError("usernameDoesNotExist");
            getContext().getValidationErrors().add("username", error);
            return getContext().getSourcePageResolution();
        } catch (EmailNotConfiguredException ex) {
            Logger.getLogger(RequestNewPasswordActionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new RedirectResolution(LoginActionBean.class);
    }
}
