/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.web.ui.stripes;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.ejb.entity.schema.Residence;
import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.biz.AsemoBizWrapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.controller.LifecycleStage;

/**
 *
 * @author Juha Loukkola
 */
public class ResidenceSettingsActionBean extends AsemoActionBean {

    private AsemoBizWrapper bizWrapper;
    private Business biz;
//    private String buildingType;
//    private int numResidents;
//    private double grossArea;
//    private double buildingVolume;
//    private double certifiedEnergyConsumption;
//    private int yearOfCompletion;
//    private int yearOfrenovation;
//    private String heatingMethod;
//    private String insulation;
//    private String heatRecovery;
//    private boolean heatPump;
//    private String alterationWork;
//    private String heatRecoveryMachine;
//    private String conversionWork;
//    private String mainInsulationMaterial;
    private String otherAdditionalEnergySupply;
    private Residence residence;
    private static Map<String, String> buildingTypes = new HashMap<String, String>();

    static {
        buildingTypes.put("ot", "omakotitalo");
        buildingTypes.put("pt", "paritalo");
        buildingTypes.put("rt", "rivitalo");
        buildingTypes.put("kt", "kerrostalo");
    }
    private static Map<String, String> heatingMethods = new HashMap<String, String>();

    static {
        heatingMethods.put("kl", "kaukolämpö");
        heatingMethods.put("öl", "öljy");
        heatingMethods.put("ka", "kaasu");
        heatingMethods.put("pk", "puukattila");
        heatingMethods.put("pl", "pelletti");
        heatingMethods.put("ss", "suora sähkö");
    }
    private static Map<String, String> additionalEnergySupplyTypes = new HashMap<String, String>();

    static {
        additionalEnergySupplyTypes.put("heatStoringFireplace", "varaava takka");
        additionalEnergySupplyTypes.put("solarHeatCollector", "aurinkokerääjä");
        additionalEnergySupplyTypes.put("solarPanel", "aurinkopaneeli");
        additionalEnergySupplyTypes.put("windMill", "tuulivoimala");
        additionalEnergySupplyTypes.put("airHeatPump", "ilmalämpöpumppu");
        additionalEnergySupplyTypes.put("airWaterHeatPump", "ilma-vesilämpöpumppu");
        additionalEnergySupplyTypes.put("outgoingAirHeatPump", "poistoilman lämpöpumppu");
        additionalEnergySupplyTypes.put("battery", "akku");
        additionalEnergySupplyTypes.put("electricCar", "sähkoauto");
    }

    public ResidenceSettingsActionBean() {
        this.bizWrapper = new AsemoBizWrapper();
        this.biz = this.bizWrapper.getBusinessBean();
    }

    @Before(stages = LifecycleStage.BindingAndValidation)
    public void rehydrate() {
        User u = this.biz.getUser(this.getContext().getUser().getId());
        this.residence = u.getCustomer().getResidence();
        //this.residence = this.getContext().getUser().getCustomer().getResidence();
//        if (this.residence == null) {
//            this.residence = new Residence();
//            this.residence.setCustomer(u.getCustomer());
//            this.biz.addResidence(residence);
//            //this.biz.getUser(this.getContext().getUser().getId()).getCustomer().setResidence(this.residence);
//            //this.getContext().getUser().getCustomer().setResidence(this.residence);
//        }
    }

    public Residence getResidence() {
        return residence;
    }

    public void setResidence(Residence residence) {
        this.residence = residence;
    }

//    public String getAlterationWork() {    
//        return alterationWork;
//    }
//
//    public void setAlterationWork(String alterationWork) {
//        this.alterationWork = alterationWork;
//    }
    public int getBuildingVolume() {
        return this.residence.getBuildingVolume();
    }

    public void setBuildingVolume(int buildingVolume) {
        this.residence.setBuildingVolume(buildingVolume);
    }

    public int getCertifiedEnergyConsumption() {
        return this.residence.getCertifiedEnergyConsumption();
    }

    public void setCertifiedEnergyConsumption(int certifiedEnergyConsumption) {
        this.residence.setCertifiedEnergyConsumption(certifiedEnergyConsumption);
    }

    public int getGrossArea() {
        return this.residence.getGrossArea();
    }

    public void setGrossArea(int grossArea) {
        this.residence.setGrossArea(grossArea);
    }

//    public boolean isHeatPump() {
//        return heatPump;
//    }
//
//    public void setHeatPump(boolean heatPump) {
//        this.heatPump = heatPump;
//    }
//    public String getHeatRecovery() {
//        return heatRecovery;
//    }
//
//    public void setHeatRecovery(String heatRecovery) {
//        this.heatRecovery = heatRecovery;
//    }
    public String getHeatingMethod() {
        return this.residence.getHeatingMethod();
    }

    public void setHeatingMethod(String heatingMethod) {
        this.residence.setHeatingMethod(heatingMethod);
    }

//    public String getInsulation() {
//        return insulation;
//    }
//
//    public void setInsulation(String insulation) {
//        this.insulation = insulation;
//    }
    public int getNumResidents() {
        return this.residence.getNumResidents();
    }

    public void setNumResidents(int numResidents) {
        this.residence.setNumResidents(numResidents);
    }

    public String getBuildingType() {
        return this.residence.getBuildingType();
    }

    public void setBuildingType(String type) {
        this.residence.setBuildingType(type);
    }

    public int getYearOfCompletion() {
        return this.residence.getYearOfCompletion();
    }

    public void setYearOfCompletion(int yearOfCompletion) {
        this.residence.setYearOfCompletion(yearOfCompletion);
    }

    public int getYearOfrenovation() {
        return this.residence.getYearOfrenovation();
    }

    public void setYearOfrenovation(int yearOfrenovation) {
        this.residence.setYearOfrenovation(yearOfrenovation);
    }

    public String getAdditionalInfo() {
        return this.residence.getAdditionalInfo();
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.residence.setAdditionalInfo(additionalInfo);
    }

    public Map<String, String> getBuildingTypes() {
        return buildingTypes;
    }

    public Map<String, String> getHeatingMethods() {
        return heatingMethods;
    }

    public String getConversionWork() {
        return this.residence.getConversionWork();
    }

    public void setConversionWork(String conversionWork) {
        this.residence.setConversionWork(conversionWork);
    }

    public String getHeatRecoveryMachine() {
        return this.residence.getHeatRecoveryMachine();
    }

    public void setHeatRecoveryMachine(String heatRecoveryMachine) {
        this.residence.setHeatRecoveryMachine(heatRecoveryMachine);
    }

    public String getMainInsulationMaterial() {
        return getMainInsulationMaterial();
    }

    public void setMainInsulationMaterial(String mainInsulationMaterial) {
        this.residence.setMainInsulationMaterial(mainInsulationMaterial);
    }

    public List<String> getAdditionalEnergySupplyTypes() {
        return new ArrayList<String>(additionalEnergySupplyTypes.keySet());
    }

    public List<String> getAdditionalEnergySupplyTypeDescriptions() {
        return new ArrayList<String>(additionalEnergySupplyTypes.values());
    }

    public String getOtherAdditionalEnergySupply() {
        return otherAdditionalEnergySupply;
    }

    public void setOtherAdditionalEnergySupply(String otherAdditionalEnergySupply) {
        this.otherAdditionalEnergySupply = otherAdditionalEnergySupply;
    }

    public boolean isAirHeatPump() {
        return this.residence.isAirHeatPump();
    }

    public void setAirHeatPump(boolean airHeatPump) {
        this.residence.setAirHeatPump(airHeatPump);
    }

    public boolean isAirWaterHeatPump() {
        return this.residence.isAirWaterHeatPump();
    }

    public void setAirWaterHeatPump(boolean airWaterHeatPump) {
        this.residence.setAirWaterHeatPump(airWaterHeatPump);
    }

    public boolean isBattery() {
        return this.residence.isBattery();
    }

    public void setBattery(boolean battery) {
        this.residence.setBattery(battery);
    }

    public boolean isElectricCar() {
        return this.residence.isElectricCar();
    }

    public void setElectricCar(boolean electricCar) {
        this.residence.setElectricCar(electricCar);
    }

    public boolean isHeatStoringFireplace() {
        return this.residence.isHeatStoringFireplace();
    }

    public void setHeatStoringFireplace(boolean heatStoringFireplace) {
        this.residence.setHeatStoringFireplace(heatStoringFireplace);
    }

    public boolean isOutgoingAirHeatPump() {
        return this.residence.isOutgoingAirHeatPump();
    }

    public void setOutgoingAirHeatPump(boolean outgoingAirHeatPump) {
        this.residence.setOutgoingAirHeatPump(outgoingAirHeatPump);
    }

    public boolean isSolarHeatCollector() {
        return this.residence.isSolarHeatCollector();
    }

    public void setSolarHeatCollector(boolean solarHeatCollector) {
        this.residence.setSolarHeatCollector(solarHeatCollector);
    }

    public boolean isSolarPanel() {
        return this.residence.isSolarPanel();
    }

    public void setSolarPanel(boolean solarPanel) {
        this.residence.setSolarPanel(solarPanel);
    }

    public boolean isWindMill() {
        return this.residence.isWindMill();
    }

    public void setWindMill(boolean windMill) {
        this.residence.setWindMill(windMill);
    }

    /*
     * Persists changes
     *
     * TODO: Examine use of AJAX instead of forward
     */
    public Resolution save() {
        this.biz.updateResidence(this.residence);
        return this.view();
    }

    @DefaultHandler
    public Resolution view() {
        this.getContext().getRequest().setAttribute("customer", this.biz.getUser(this.getContext().getUser().getId()).getCustomer());
        this.getContext().getRequest().setAttribute("residence", this.residence);
        this.getContext().getRequest().setAttribute("buildingTypes", this.getBuildingTypes());
        this.getContext().getRequest().setAttribute("heatingMethods", this.getHeatingMethods());
        this.getContext().getRequest().setAttribute("energySupplyTypes", this.getAdditionalEnergySupplyTypes());
        this.getContext().getRequest().setAttribute("energySupplyTypeDescriptions", this.getAdditionalEnergySupplyTypeDescriptions());
        return new ForwardResolution(Urls.USER_SETTINGS);
    }
}
