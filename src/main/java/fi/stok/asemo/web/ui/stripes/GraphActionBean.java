/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.web.ui.stripes;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.biz.AsemoBizWrapper;
import java.io.StringReader;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.StreamingResolution;
import net.sourceforge.stripes.controller.LifecycleStage;

/**
 *
 * @author Juha Loukkola
 */
public class GraphActionBean extends AsemoActionBean { //implements LogInterface {

    private AsemoBizWrapper bizWrapper;
    private Business biz;
//    private LogLevel logLevel;
//    private Date from;
//    private Date to;
//    private Collection<Instrument> instruments;
//    private Instrument instrument;

    public GraphActionBean() {
        this.bizWrapper = new AsemoBizWrapper();
        this.biz = this.bizWrapper.getBusinessBean();
    }

//    @Override
//    public Resolution getDayLog() {
//        calculateCurrentTimeWindow();
//        return getJSONLogStream();
//    }
//
//    @Override
//    public Resolution getWeekLog() {
//        calculateCurrentTimeWindow();
//        return getJSONLogStream();
//    }
//
//    @Override
//    public Resolution getMonthLog() {
//        calculateCurrentTimeWindow();
//        return getJSONLogStream();
//    }
//
//    @Override
//    public Resolution getYearLog() {
//        calculateCurrentTimeWindow();
//        return getJSONLogStream();
//    }
//    @Override
//    public Resolution getLog() {
//        calculateCurrentTimeWindow();
//        return getJSONLog();
//    }

//    @Override
//    public Resolution getUpdates() {
//        return getJSONLogStream();
//    }
//    private void calculateCurrentTimeWindow() {
//        Calendar cal = Calendar.getInstance();
//        this.to = cal.getTime();
//        int calField;
//        switch (logLevel) {
//            case day:
//                calField = Calendar.DAY_OF_YEAR;
//                break;
//            case week:
//                calField = Calendar.WEEK_OF_YEAR;
//                break;
//            case month:
//                calField = Calendar.MONTH;
//                break;
//            case year:
//                calField = Calendar.YEAR;
//                break;
//            default:
//                calField = Calendar.DAY_OF_YEAR;
//                break;
//        }
//        cal.add(calField, -1);
//        this.from = cal.getTime();
//    }
    
    private StreamingResolution getJSONLog() {
        // Get log data from LogEJB
        // Format log data into JSON
        String JSONData = "";
        return new StreamingResolution("text", new StringReader(JSONData));
    }

    @Before(stages = LifecycleStage.BindingAndValidation)
    public void rehydrate() {
        this.getContext().getRequest().setAttribute("hrs", 1);
        this.getContext().getRequest().setAttribute("streams", this.biz.getUser(this.getContext().getUser().getId()).getUserSettings().getStreams());
    }

    @DefaultHandler
    public Resolution view() {
        return new ForwardResolution(Urls.USER_GRAPH);
    }
}
