package fi.stok.asemo.web.ui.stripes.admin;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.exception.IncorrectPasswordException;
import fi.stok.asemo.biz.exception.PasswordException;
import fi.stok.asemo.biz.exception.UserNotFoundException;
import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.stripes.AsemoActionBean;
import javax.ejb.EJB;
import net.sourceforge.stripes.action.*;
import net.sourceforge.stripes.validation.LocalizableError;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationError;


/**
 *
 * @author Juha Loukkola
 */
//@UrlBinding("/admin")
public class LoginActionBean extends AsemoActionBean {

    @EJB(mappedName = "java:app/AsemoBusiness/BusinessBean!fi.stok.asemo.biz.Business")
    private Business biz;
    private String role = "ADMIN";
    //@Validate(required=true)
    private String username;
    //@Validate(required=true)
    private String password;
    private String targetUrl;

    /**
     * The username of the user trying to log in.
     */
    @Validate(required=true, on="login")
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * The username of the user trying to log in.
     */
    public String getUsername() {
        return username;
    }

    /**
     * The password of the user trying to log in.
     */
    @Validate(required=true, on="login")
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * The password of the user trying to log in.
     */
    public String getPassword() {
        return password;
    }

    /**
     * The URL the user was trying to access (null if the login page was
     * accessed directly).
     */
    public String getTargetUrl() {
        return targetUrl;
    }

    /**
     * The URL the user was trying to access (null if the login page was
     * accessed directly).
     */
    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }

    public Resolution login() throws PasswordException, ClassNotFoundException {
        User user;
        // Authenticate user
        try {
            user = this.biz.authenticateUser(username, password);
        } catch (UserNotFoundException ex) {
            ValidationError error = new LocalizableError("usernameDoesNotExist");
            getContext().getValidationErrors().add("username", error);
            return getContext().getSourcePageResolution();
            //Logger.getLogger(LoginActionBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IncorrectPasswordException ex) {
            ValidationError error = new LocalizableError("incorrectPassword");
            getContext().getValidationErrors().add("password", error);
            return getContext().getSourcePageResolution();
            //Logger.getLogger(LoginActionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        // Check admin privileges
        if (!this.biz.userInRole(user, role)) {
            ValidationError error = new LocalizableError("insufficientPrivileges");
            getContext().getValidationErrors().add("username", error);
            return getContext().getSourcePageResolution();
        }
        // Set user and its role to the session context 
        getContext().setAdmin(user);
        // Redirect to a page to be shown after succesful login
        if (this.targetUrl != null) {
            return new RedirectResolution(this.targetUrl);
        } else {
            return new RedirectResolution(Class.forName(Urls.ADMIN_START_ACTION).asSubclass(ActionBean.class));
//            return new RedirectResolution(CustomersActionBean.class);
        }
    }

    @DefaultHandler
    public Resolution view() {
        return new ForwardResolution(Urls.ADMIN_LOGIN);
    }
}
