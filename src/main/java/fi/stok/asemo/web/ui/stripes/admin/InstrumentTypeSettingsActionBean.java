package fi.stok.asemo.web.ui.stripes.admin;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.ejb.instruments.Preprocessor;
import fi.stok.asemo.biz.ejb.entity.schema.DataSchema;
import fi.stok.asemo.biz.ejb.entity.schema.DataSourceType;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.biz.AsemoBizWrapper;
import fi.stok.asemo.web.ui.stripes.AsemoActionBean;
import java.util.List;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;

/**
 * @author Juha Loukkola
 */
public class InstrumentTypeSettingsActionBean extends AsemoActionBean {

    private DataSourceType instrumentType;
    private Business biz = new AsemoBizWrapper().getBusinessBean();
    private List<DataSchema> dataSchemas;
    private DataSchema schema;
    private List<String> preprocessors;

    public DataSourceType getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(DataSourceType instrumentType) {
        this.instrumentType = instrumentType;
    }

    public List<DataSchema> getDataSchemas() {
        return dataSchemas;
    }

    public void setDataSchemas(List<DataSchema> dataSchemas) {
        this.dataSchemas = dataSchemas;
    }

    public Integer getSchemaId() {
        if (this.schema != null) {
            return this.schema.getId();
        }
        return null;
    }

    public void setSchemaId(Integer schemaId) {
        if (this.schema == null || this.schema.getId() != schemaId) {
            this.schema = this.biz.getDataSchema(schemaId);
        }
    }

    public List<String> getPreprocessors() {
        return preprocessors;
    }

    public void setPreprocessors(List<String> preprocessors) {
        this.preprocessors = preprocessors;
    }

    // TODO: May be declared as a pre-action 
    public Resolution preEdit() {
        this.instrumentType = this.biz.getDataSourceType(instrumentType.getId());
        this.dataSchemas = this.biz.getDataSchemas();
        this.preprocessors = Preprocessor.subClasses;
        this.setSchemaId(this.instrumentType.getDataSchema().getId());
        return new ForwardResolution(Urls.ADMIN_INSTRUMENT_TYPE_SETTINGS);
    }

    // TODO: Add validation
    public Resolution save() {
        this.instrumentType.setDataSchema(this.schema);
        this.biz.createDataSourceType(this.instrumentType);
        return new RedirectResolution(InstrumentTypesActionBean.class);
    }

    public Resolution cancel() {
        return new RedirectResolution(InstrumentTypesActionBean.class);
    }

    @DefaultHandler
    public Resolution view() {
        this.dataSchemas = this.biz.getDataSchemas();
        this.preprocessors = Preprocessor.subClasses;
        return new ForwardResolution(Urls.ADMIN_INSTRUMENT_TYPE_SETTINGS);
    }
}
