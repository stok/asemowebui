package fi.stok.asemo.web.ui.stripes;

import fi.stok.asemo.biz.ejb.entity.schema.User;
import net.sourceforge.stripes.action.ActionBeanContext;

/**
 * ActionBeanContext subclass for the Asemo Web UI application that manages
 * where values like the logged in user are stored.
 *
 * @author Juha Loukkola
 */
public class AsemoActionBeanContext extends ActionBeanContext {

    /**
     * Gets the currently logged in user, or null if no-one is logged in.
     */
    public User getUser() {
        return (User) getRequest().getSession().getAttribute("user");
    }

    /**
     * Sets the currently logged in user.
     */
    public void setUser(User currentUser, String currentRole) {
        getRequest().getSession().setAttribute("user", currentUser);
        getRequest().getSession().setAttribute("role", currentRole);
    }

    public void setAdmin(User adminUser) {
        getRequest().getSession().setAttribute("admin", adminUser);
    }

    public User getAdmin() {
        return (User) getRequest().getSession().getAttribute("admin");
    }

    public void logout() {
        if (this.getAdmin() != null) {
            getRequest().getSession().removeAttribute("user");
            getRequest().getSession().removeAttribute("role");
        } else {
            getRequest().getSession().invalidate();
        }
    }

    /**
     * Logs the user out by invalidating the session.
     */
    public void adminLogout() {
        getRequest().getSession().invalidate();
    }
}
