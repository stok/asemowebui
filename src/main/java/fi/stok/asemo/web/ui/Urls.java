package fi.stok.asemo.web.ui;

import fi.stok.asemo.web.ui.stripes.*;
import fi.stok.asemo.web.ui.stripes.admin.CustomerSettingsActionBean;
import fi.stok.asemo.web.ui.stripes.admin.InstrumentTypeSettingsActionBean;
import fi.stok.asemo.web.ui.stripes.admin.UserSettingsActionBean;

/**
 * NOTE!!! Getters should not be static methods.
 *
 * @author Juha Loukkola
 */
public class Urls {

    // User ActionBeans
    public static final String USER_USER_LOGOUT_ACTION = LogoutActionBean.class.getName();
    public static final String USER_SETTINGS_ACTION = ResidenceSettingsActionBean.class.getName();
    public static final String USER_GRAPH_ACTION = GraphActionBean.class.getName();
    public static final String USER_LOGIN_ACTION = LoginActionBean.class.getName();
    public static final String USER_START_ACTION = USER_GRAPH_ACTION;
    public static final String USER_HISTORY_ACTION = HistoryActionBean.class.getName();
    public static final String USER_CHANGE_PASSWORD_ACTION = ChangePasswordActionBean.class.getName();
    public static final String USER_CHANGE_INFO_ACTION = ChangeUserInfoActionBean.class.getName();
    public static final String USER_REQUEST_PASSWORD_ACTION = RequestNewPasswordActionBean.class.getName();
    // User pages
    public static final String USER_CONTENT_ROOT = "/content";
    public static final String USER_GRAPH = USER_CONTENT_ROOT + "/Graph2.jsp";
    public static final String USER_COMPARISON_GRAPH = USER_CONTENT_ROOT + "/ComparisonGraph.jsp";
    public static final String USER_BLOG = USER_CONTENT_ROOT + "/Blog.jsp";
    public static final String USER_LOGIN = "/index.jsp";
    public static final String USER_LOGOUT = "/Logout.action";
    public static final String USER_START = USER_GRAPH;
    public static final String USER_HISTORY = USER_CONTENT_ROOT + "/History.jsp";
    public static final String USER_SETTINGS = USER_CONTENT_ROOT + "/UserSettings.jsp";
    public static final String USER_CHANGE_PASSWORD = USER_CONTENT_ROOT + "/ChangePassword.jsp";
    public static final String USER_CHANGE_INFO = USER_CONTENT_ROOT + "/ChangeUserInfo.jsp";
    public static final String USER_REQUEST_PASSWORD = USER_CONTENT_ROOT + "/RequestNewPassword.jsp";
    // Admin ActionBeans
    public static final String ADMIN_INSTRUMENT_TYPE_SETTINGS_ACTION = InstrumentTypeSettingsActionBean.class.getName();
    public static final String ADMIN_CUSTOMERS_ACTION = fi.stok.asemo.web.ui.stripes.admin.CustomersActionBean.class.getName();
    public static final String ADMIN_CUSTOMER_SETTINGS_ACTION = CustomerSettingsActionBean.class.getName();
    public static final String ADMIN_USER_SETTINGS_ACTION = UserSettingsActionBean.class.getName();
    public static final String ADMIN_LOGIN_ACTION = fi.stok.asemo.web.ui.stripes.admin.LoginActionBean.class.getName();
    public static final String ADMIN_NODE_SETTINGS_ACTION = fi.stok.asemo.web.ui.stripes.admin.NodeSettingsActionBean.class.getName();
    public static final String ADMIN_INSTRUMENT_SETTINGS_ACTION = fi.stok.asemo.web.ui.stripes.admin.InstrumentSettingsActionBean.class.getName();
    public static final String ADMIN_LIST_DATA_SCHEMA_SETTINGS_ACTION = fi.stok.asemo.web.ui.stripes.admin.ListDataSchemaSettingsActionBean.class.getName();
    public static final String ADMIN_SCHEMA_ELEMENT_SETTINGS_ACTION = fi.stok.asemo.web.ui.stripes.admin.SchemaElementSettingsActionBean.class.getName();
    public static final String ADMIN_START_ACTION = ADMIN_CUSTOMERS_ACTION;
    public static final String ADMIN_LOGOUT_ACTION = fi.stok.asemo.web.ui.stripes.admin.LogoutActionBean.class.getName();
    public static final String ADMIN_USER_MONITOR_ACTION = fi.stok.asemo.web.ui.stripes.admin.UserMonitorActionBean.class.getName();
    // Admin pages
    public static final String ADMIN_CONTENT_ROOT = "/admin";
    public static final String ADMIN_LOGIN = ADMIN_CONTENT_ROOT + "/index.jsp";
    public static final String ADMIN_CUSTOMERS = ADMIN_CONTENT_ROOT + "/Customers.jsp";
    public static final String ADMIN_USERS = ADMIN_CONTENT_ROOT + "/Users.jsp";
    public static final String ADMIN_NODES = ADMIN_CONTENT_ROOT + "/Nodes.jsp";
    public static final String ADMIN_CUSTOMER_SETTINGS = ADMIN_CONTENT_ROOT + "/CustomerSettings.jsp";
    public static final String ADMIN_USER_SETTINGS = ADMIN_CONTENT_ROOT + "/UserSettings.jsp";
    public static final String ADMIN_NODE_SETTINGS = ADMIN_CONTENT_ROOT + "/NodeSettings.jsp";
    public static final String ADMIN_INSTRUMENT_TYPE_SETTINGS = ADMIN_CONTENT_ROOT + "/InstrumentTypeSettings.jsp";
    public static final String ADMIN_INSTRUMENT_TYPES = ADMIN_CONTENT_ROOT + "/InstrumentTypes.jsp";
    public static final String ADMIN_START = ADMIN_CUSTOMERS;
    public static final String ADMIN_INSTRUMENT_SETTINGS = ADMIN_CONTENT_ROOT + "/InstrumentSettings.jsp";
    public static final String ADMIN_INSTRUMENTS = ADMIN_CONTENT_ROOT + "/Instruments.jsp";
    public static final String ADMIN_DATA_SCHEMAS = ADMIN_CONTENT_ROOT + "/DataSchemas.jsp";
    public static final String ADMIN_LIST_DATA_SCHEMA_SETTINGS = ADMIN_CONTENT_ROOT + "/ListDataSchemaSettings.jsp";
    public static final String ADMIN_SCHEMA_ELEMENT_SETTINGS = ADMIN_CONTENT_ROOT + "/SchemaElementSettings.jsp";

    public Urls() {
    }

    public String getADMIN_CONTENT_ROOT() {
        return ADMIN_CONTENT_ROOT;
    }

    public String getADMIN_CUSTOMERS() {
        return ADMIN_CUSTOMERS;
    }

    public String getADMIN_CUSTOMER_SETTINGS() {
        return ADMIN_CUSTOMER_SETTINGS;
    }

    public String getADMIN_CUSTOMER_SETTINGS_ACTION() {
        return ADMIN_CUSTOMER_SETTINGS_ACTION;
    }

    public String getADMIN_NODES() {
        return ADMIN_NODES;
    }

    public String getADMIN_NODE_SETTINGS() {
        return ADMIN_NODE_SETTINGS;
    }

    public String getADMIN_USER_SETTINGS() {
        return ADMIN_USER_SETTINGS;
    }

    public String getADMIN_USERS() {
        return ADMIN_USERS;
    }

    public String getUSER_BLOG() {
        return USER_BLOG;
    }

    public String getUSER_COMPARISON_GRAPH() {
        return USER_COMPARISON_GRAPH;
    }

    public String getUSER_CONTENT_ROOT() {
        return USER_CONTENT_ROOT;
    }

    public String getUSER_GRAPH() {
        return USER_GRAPH;
    }

    public String getUSER_LOGOUT() {
        return USER_LOGOUT;
    }

    public String getUSER_START() {
        return USER_START;
    }

    public String getUSER_GRAPH_ACTION() {
        return USER_GRAPH_ACTION;
    }

    public String getUSER_USER_LOGOUT_ACTION() {
        return USER_USER_LOGOUT_ACTION;
    }

    public String getUSER_SETTINGS_ACTION() {
        return USER_SETTINGS_ACTION;
    }

    public String getUSER_LOGIN() {
        return USER_LOGIN;
    }

    public String getUSER_LOGIN_ACTION() {
        return USER_LOGIN_ACTION;
    }

    public String getADMIN_INSTRUMENT_TYPE_SETTINGS_ACTION() {
        return ADMIN_INSTRUMENT_TYPE_SETTINGS_ACTION;
    }

    public String getADMIN_INSTRUMENT_TYPES() {
        return ADMIN_INSTRUMENT_TYPES;
    }

    public String getADMIN_INSTRUMENT_TYPE_SETTINGS() {
        return ADMIN_INSTRUMENT_TYPE_SETTINGS;
    }

    public String getADMIN_USER_SETTINGS_ACTION() {
        return ADMIN_USER_SETTINGS_ACTION;
    }

    public String getADMIN_LOGIN() {
        return ADMIN_LOGIN;
    }

    public String getADMIN_LOGIN_ACTION() {
        return ADMIN_LOGIN_ACTION;
    }

    public String getADMIN_NODE_SETTINGS_ACTION() {
        return ADMIN_NODE_SETTINGS_ACTION;
    }

    public String getADMIN_START() {
        return ADMIN_START;
    }

    public String getUSER_START_ACTION() {
        return USER_START_ACTION;
    }

    public String getADMIN_INSTRUMENT_SETTINGS_ACTION() {
        return ADMIN_INSTRUMENT_SETTINGS_ACTION;
    }

    public String getADMIN_INSTRUMENTS() {
        return ADMIN_INSTRUMENTS;
    }

    public String getADMIN_INSTRUMENT_SETTINGS() {
        return ADMIN_INSTRUMENT_SETTINGS;
    }

    public String getADMIN_LOGOUT_ACTION() {
        return ADMIN_LOGOUT_ACTION;
    }

    public String getUSER_HISTORY() {
        return USER_HISTORY;
    }

    public String getUSER_HISTORY_ACTION() {
        return USER_HISTORY_ACTION;
    }

    public String getADMIN_DATA_SCHEMAS() {
        return ADMIN_DATA_SCHEMAS;
    }

    public String getADMIN_LIST_DATA_SCHEMA_SETTINGS_ACTION() {
        return ADMIN_LIST_DATA_SCHEMA_SETTINGS_ACTION;
    }

    public String getUSER_CHANGE_PASSWORD() {
        return USER_CHANGE_PASSWORD;
    }

    public String getUSER_CHANGE_PASSWORD_ACTION() {
        return USER_CHANGE_PASSWORD_ACTION;
    }

    public String getUSER_SETTINGS() {
        return USER_SETTINGS;
    }

    public String getUSER_CHANGE_INFO_ACTION() {
        return USER_CHANGE_INFO_ACTION;
    }

    public String getUSER_CHANGE_INFO() {
        return USER_CHANGE_INFO;
    }

    public String getUSER_REQUEST_PASSWORD() {
        return USER_REQUEST_PASSWORD;
    }

    public String getUSER_REQUEST_PASSWORD_ACTION() {
        return USER_REQUEST_PASSWORD_ACTION;
    }

    public String getADMIN_LIST_DATA_SCHEMA_SETTINGS() {
        return ADMIN_LIST_DATA_SCHEMA_SETTINGS;
    }

    public String getADMIN_SCHEMA_ELEMENT_SETTINGS() {
        return ADMIN_SCHEMA_ELEMENT_SETTINGS;
    }

    public String getADMIN_SCHEMA_ELEMENT_SETTINGS_ACTION() {
        return ADMIN_SCHEMA_ELEMENT_SETTINGS_ACTION;
    }

    public String getADMIN_START_ACTION() {
        return ADMIN_START_ACTION;
    }

    public String getADMIN_USER_MONITOR_ACTION() {
        return ADMIN_USER_MONITOR_ACTION;
    }
    
}
