/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.web.ui;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.Initializer;
import fi.stok.asemo.biz.exception.PasswordException;
import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.biz.ejb.entity.schema.UserGroup;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.servlet.ServletContextEvent;

/**
 *
 * @author Juha Loukkola
 */
//@WebListener 
public class StartupListener implements javax.servlet.ServletContextListener {

    private static final String USER_GROUPS = "ADMIN,USER,DEMO";
    private static final String ADMIN_GROUP = "ADMIN";
    private static final String ADMIN_USER = "admin";
    private static final String ADMIN_PASSWORD = "salasana";
    @Resource(name = "AsemoInitializationSettings")
    private Properties initializationSettings;
    @EJB
    private Business biz;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        // Setup user groups(roles)
        String str = initializationSettings.getProperty("userGroups", StartupListener.USER_GROUPS);
        String[] roles = str.split("[,]");
        for (String role : roles) {
            if (this.biz.getUserGroup(role) == null) {
                UserGroup group = new UserGroup(role);
                this.biz.addUserGroup(group);
            }
        }

        // Setup admin user
        String adminUser = this.initializationSettings.getProperty("adminUser", StartupListener.ADMIN_USER);
        String adminPassword = this.initializationSettings.getProperty("adminPassword", StartupListener.ADMIN_PASSWORD);
        String adminGroup = this.initializationSettings.getProperty("adminGroup", StartupListener.ADMIN_GROUP);
        // Check if admin user is already been created if not create a new
        User admin = this.biz.getUser(adminUser);
        if (admin == null) {
            try {
                admin = new User(adminUser, adminPassword);
                this.biz.addUser(admin);
                this.biz.addUserIntoRole(admin, this.biz.getUserGroup(adminGroup));
            } catch (PasswordException ex) {
                Logger.getLogger(Initializer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
