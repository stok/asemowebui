package fi.stok.asemo.web.ui.stripes;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;


/**
 * Simple ActionBean implementation that all ActionBeans in the Asemo Web UI
 * will extend.
 *
 * @author Juha Loukklpa
 */
public abstract class AsemoActionBean implements ActionBean {
    private AsemoActionBeanContext context;

    @Override
    public void setContext(ActionBeanContext context) {
        this.context = (AsemoActionBeanContext) context;
    }

    /** Gets the ActionBeanContext set by Stripes during initialization. */
    @Override
    public AsemoActionBeanContext getContext() {
        return this.context;
    }
}
