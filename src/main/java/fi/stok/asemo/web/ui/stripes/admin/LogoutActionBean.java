package fi.stok.asemo.web.ui.stripes.admin;

import fi.stok.asemo.web.ui.stripes.AsemoActionBean;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;

/**
 * Straightforward logout action that logs the user out and then sends to an
 * exit page.
 *
 * @author Tim Fennell
 */
public class LogoutActionBean extends AsemoActionBean {

    public Resolution logout() throws Exception {
        getContext().adminLogout();
        return new RedirectResolution(fi.stok.asemo.web.ui.stripes.admin.LoginActionBean.class);
    }
}