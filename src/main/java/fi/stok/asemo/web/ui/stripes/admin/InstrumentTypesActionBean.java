/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.web.ui.stripes.admin;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.ejb.entity.schema.DataSourceType;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.biz.AsemoBizWrapper;
import fi.stok.asemo.web.ui.stripes.AsemoActionBean;
import java.util.List;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.validation.Validate;

/**
 *
 * @author Juha Loukkola
 */
public class InstrumentTypesActionBean extends AsemoActionBean {

    private Business biz;
    private List<DataSourceType> instrumentTypes;
    @Validate(on = "remove", required = true)
    private int[] deleteIds;

    public InstrumentTypesActionBean() {
        this.biz = new AsemoBizWrapper().getBusinessBean();
    }

    public int[] getDeleteIds() {
        return deleteIds;
    }

    public void setDeleteIds(int[] deleteIds) {
        this.deleteIds = deleteIds;
    }

    public List<DataSourceType> getInstrumentTypes() {
        return instrumentTypes;
    }

    public void setInstrumentTypes(List<DataSourceType> instrumentTypes) {
        this.instrumentTypes = instrumentTypes;
    }

    @Before(stages = LifecycleStage.BindingAndValidation)
    public void rehydrate() {
        this.instrumentTypes = this.biz.getDataSourceTypes();
        this.getContext().getRequest().setAttribute("urls", new Urls());
    }

    public Resolution create() {
        return new RedirectResolution(InstrumentTypeSettingsActionBean.class);
    }

    public Resolution remove() {
        for (int id : this.deleteIds) {
            this.biz.deleteDataSourceType(id);
        }
        return new RedirectResolution(InstrumentTypesActionBean.class);
    }

    @DefaultHandler
    public Resolution view() {
        return new ForwardResolution(Urls.ADMIN_INSTRUMENT_TYPES);
    }
}
