/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.web.ui.stripes;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.biz.AsemoBizWrapper;
import net.sourceforge.stripes.action.*;
import net.sourceforge.stripes.controller.LifecycleStage;

/**
 *
 * @author Juha Loukkola
 */
public class HistoryActionBean extends AsemoActionBean {

    private Business biz;
//    private Collection<Instrument> instruments;
//    private Instrument instrument;

    public HistoryActionBean() {
        this.biz = new AsemoBizWrapper().getBusinessBean();
    }

    @Before(stages = LifecycleStage.BindingAndValidation)
    public void rehydrate() {
        this.getContext().getRequest().setAttribute("streams", this.biz.getUser(this.getContext().getUser().getId()).getUserSettings().getStreams());
//        User u = this.biz.getUser(this.getContext().getUser().getId());
    }

    @DefaultHandler
    public Resolution view() {
        return new ForwardResolution(Urls.USER_HISTORY);
    }
}
