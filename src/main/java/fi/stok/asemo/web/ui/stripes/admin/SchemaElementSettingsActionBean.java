/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.web.ui.stripes.admin;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.ejb.entity.schema.VariableType;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.biz.AsemoBizWrapper;
import fi.stok.asemo.web.ui.stripes.AsemoActionBean;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;

/**
 *
 * @author Juha Loukkola
 */
public class SchemaElementSettingsActionBean extends AsemoActionBean{

    private Business biz;
    private VariableType element;

    public SchemaElementSettingsActionBean() {
        this.biz = new AsemoBizWrapper().getBusinessBean();
    }

    public VariableType getElement() {
        return element;
    }

    public void setElement(VariableType element) {
        this.element = element;
    }
      
    public Resolution preEdit() {      
        this.element = this.biz.getSchemaElement(this.element.getId());
        return new ForwardResolution(Urls.ADMIN_SCHEMA_ELEMENT_SETTINGS);
    }
    
    public Resolution save() {
        RedirectResolution res = new RedirectResolution(ListDataSchemaSettingsActionBean.class);
        res.addParameter("preEdit", "");
        res.addParameter("element.id", this.element.getId());
        return res;
    }
    
    public Resolution cancel() {
        RedirectResolution res = new RedirectResolution(ListDataSchemaSettingsActionBean.class);
        res.addParameter("preEdit", "");
        res.addParameter("element.id", this.element.getId());
        return res;
    }
    
    @DefaultHandler
    public Resolution view() {
        return new ForwardResolution(Urls.ADMIN_SCHEMA_ELEMENT_SETTINGS);
    }
}
