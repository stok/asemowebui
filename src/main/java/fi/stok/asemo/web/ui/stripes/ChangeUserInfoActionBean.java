
package fi.stok.asemo.web.ui.stripes;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.ejb.entity.schema.Customer;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.biz.AsemoBizWrapper;
import java.security.NoSuchAlgorithmException;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;

/**
 *
 * @author Juha Loukkola
 */
public class ChangeUserInfoActionBean extends AsemoActionBean {

    private String streetAddress;
    private String zipCode;
    private String city;
    private String email;
    private String phone;
    private Integer id;
    private Customer customer;

    private AsemoBizWrapper bizWrapper;
    private Business biz;

    public ChangeUserInfoActionBean() {
        this.bizWrapper = new AsemoBizWrapper();
        this.biz = this.bizWrapper.getBusinessBean();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @DefaultHandler
    public Resolution view() {
        this.customer = this.biz.getUser(this.getId()).getCustomer();
        return new ForwardResolution(Urls.USER_CHANGE_INFO);
    }

    public Resolution save() throws NoSuchAlgorithmException {
        this.customer = this.biz.getUser(this.getId()).getCustomer();
        customer.setStreetAddress(streetAddress);
        customer.setZipCode(zipCode);
        customer.setCity(city);
        customer.setEmail(email);
        customer.setPhone(phone);
        this.biz.saveCustomer(customer);
        return new RedirectResolution(ResidenceSettingsActionBean.class);
    }
}
