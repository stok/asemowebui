/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.web.ui.stripes.admin;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.ejb.entity.schema.Customer;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.biz.AsemoBizWrapper;
import fi.stok.asemo.web.ui.stripes.AsemoActionBean;
import java.util.List;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.validation.Validate;

/**
 *
 * @author Juha Loukkola
 */
public class CustomersActionBean extends AsemoActionBean {

    private Business biz = new AsemoBizWrapper().getBusinessBean();
    private List<Customer> customers;
    @Validate(on = "remove", required = true)
    private int[] deleteIds;

    public int[] getDeleteIds() {
        return deleteIds;
    }

    public void setDeleteIds(int[] deleteIds) {
        this.deleteIds = deleteIds;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    @Before(stages = LifecycleStage.BindingAndValidation)
    public void rehydrate() {
        this.getContext().getRequest().setAttribute("urls", new Urls());
        this.customers = this.biz.getCustomers();
    }

    public Resolution create() {
        return new RedirectResolution(CustomerSettingsActionBean.class);
    }

    public Resolution remove() {
        for (int id : this.deleteIds) {
            this.biz.deleteCustomer(id);
        }
        return this.view();
    }

    @DefaultHandler
    public Resolution view() {
        return new ForwardResolution(Urls.ADMIN_CUSTOMERS);
    }
}
