package fi.stok.asemo.web.ui.stripes;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.exception.IncorrectPasswordException;
import fi.stok.asemo.biz.exception.PasswordException;
import fi.stok.asemo.biz.exception.UserNotFoundException;
import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.web.ui.Urls;
import javax.ejb.EJB;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.validation.LocalizableError;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationError;

/**
 *
 * @author Juha Loukkola
 */
public class LoginActionBean extends AsemoActionBean {

    @EJB(mappedName = "java:app/AsemoBusiness/BusinessBean!fi.stok.asemo.biz.Business")
    private Business biz;
    private User user;
    private String username;
    private String password;
    private String targetUrl;

    /**
     * The username of the user trying to log in.
     */
    @Validate(required = true, on = "login")
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * The username of the user trying to log in.
     */
    public String getUsername() {
        return username;
    }

    /**
     * The password of the user trying to log in.
     */
    @Validate(required = true, on = "login")
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * The password of the user trying to log in.
     */
    public String getPassword() {
        return password;
    }

    /**
     * The URL the user was trying to access (null if the login page was
     * accessed directly).
     */
    public String getTargetUrl() {
        return targetUrl;
    }

    /**
     * The URL the user was trying to access (null if the login page was
     * accessed directly).
     */
    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }

    public Resolution login() throws PasswordException {
        // Authenticate user
        try {
            this.user = this.biz.authenticateUser(username, password);
        } catch (UserNotFoundException ex) {
            ValidationError error = new LocalizableError("usernameDoesNotExist");
            getContext().getValidationErrors().add("username", error);
            return getContext().getSourcePageResolution();
            //Logger.getLogger(LoginActionBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IncorrectPasswordException ex) {
            ValidationError error = new LocalizableError("incorrectPassword");
            getContext().getValidationErrors().add("password", error);
            return getContext().getSourcePageResolution();
            //Logger.getLogger(LoginActionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        // Check user privileges
        if (!this.biz.userInRole(user, "USER")) {
            ValidationError error = new LocalizableError("insufficientPrivileges");
            getContext().getValidationErrors().add("username", error);
            return getContext().getSourcePageResolution();
        }
        // Set user to the session context
        getContext().setUser(user, "USER");
        // Redirect to a page to be shown after succesful login
        if (this.targetUrl != null) {
            return new RedirectResolution(this.targetUrl);
        } else {
            // Redirect to Graph
            return new RedirectResolution(GraphActionBean.class);
        }

    }

    public Resolution demoLogin() {
        this.user = this.biz.getUser(username);

        // Check user privileges
        if (!this.biz.userInRole(user, "DEMO")) {        
            // TODO: Solave a BUG with a SourcePageResolution
            ValidationError error = new LocalizableError("insufficientPrivileges");
            getContext().getValidationErrors().add(username, error);
            //return new RedirectResolution(Urls.USER_LOGIN);
            return getContext().getSourcePageResolution();
        }
        // Set user to the session context
        getContext().setUser(user, "DEMO");
        // Redirect to a page to be shown after succesful login
        if (this.targetUrl != null) {
            return new RedirectResolution(this.targetUrl);
        } else {
            // Redirect to Graph
            return new RedirectResolution(GraphActionBean.class);
        }

    }

    @DefaultHandler
    public Resolution view() {
        return new ForwardResolution(Urls.USER_LOGIN);
    }

}
