/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.web.ui.stripes.admin;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.ejb.entity.schema.Customer;
import fi.stok.asemo.biz.ejb.entity.schema.Node;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.biz.AsemoBizWrapper;
import fi.stok.asemo.web.ui.stripes.AsemoActionBean;
import java.util.List;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.controller.LifecycleStage;

/**
 *
 * @author Juha Loukkola
 */
public class NodeSettingsActionBean extends AsemoActionBean {

    private Node node;
    private List<Customer> customers;
    private Customer customer;
    private Business biz;

    public NodeSettingsActionBean() {
        this.biz = new AsemoBizWrapper().getBusinessBean();
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Before(stages = LifecycleStage.BindingAndValidation)
    public void rehydrate() {
        String nodeId = this.getContext().getRequest().getParameter("node.id");
        if (nodeId != null && !nodeId.isEmpty()) {
            this.node = this.biz.getNode(Integer.parseInt(nodeId));
        }
    }

    public Resolution preEdit() {
        this.node = this.biz.getNode(node.getId());
        this.loadDomainObjectsForView();
        return new ForwardResolution(Urls.ADMIN_NODE_SETTINGS);
    }

    public Resolution save() {
//        // If the owner(customer) has changed
//        if (!(this.customer == this.node.getCustomer()
//                || (this.customer != null
//                && this.node.getCustomer() != null
//                && this.customer.equals(this.node.getCustomer())))) {
//
//            // Detach node from the previous owner(customer)
//            if (this.node.getCustomer() != null) {
//                this.node.getCustomer().getNodes().remove(this.node);
//                // Persist changes!
//                this.biz.saveCustomer(node.getCustomer());
//                this.node.setCustomer(null);
//            }
//
//            // Attach node to a new customer
//            if (this.customer != null) {
//                this.customer = this.biz.getCustomer(customer.getId());
//                this.node.setCustomer(customer);
//                this.customer.getNodes().add(node);
//                this.biz.saveCustomer(customer);
//            }
//        }
//        this.biz.saveNode(this.node);
        this.node.setCustomer(customer);
        this.biz.updateNode(node);
        return new RedirectResolution("/admin/Nodes.action");
    }

    public Resolution cancel() {
        return new RedirectResolution("/admin/Nodes.action");
    }

    @DefaultHandler
    public Resolution view() {
        this.loadDomainObjectsForView();
        return new ForwardResolution(Urls.ADMIN_NODE_SETTINGS);
    }

    private void loadDomainObjectsForView() {
        this.customers = this.biz.getCustomers();
    }
}
