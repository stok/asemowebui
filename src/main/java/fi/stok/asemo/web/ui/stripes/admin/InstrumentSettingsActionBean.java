/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.web.ui.stripes.admin;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.exception.UuidNotUniqueException;
import fi.stok.asemo.biz.ejb.entity.schema.DataSourceType;
import fi.stok.asemo.biz.ejb.entity.schema.Instrument;
import fi.stok.asemo.biz.ejb.entity.schema.Node;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.biz.AsemoBizWrapper;
import fi.stok.asemo.web.ui.stripes.AsemoActionBean;
import java.util.ArrayList;
import java.util.List;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.validation.LocalizableError;
import net.sourceforge.stripes.validation.ValidationErrors;

/**
 *
 * @author Juha Loukkola
 */
public class InstrumentSettingsActionBean extends AsemoActionBean {

    private Business biz;
    private Instrument instrument;
//    @Validate(on = "create", required = true)
    private DataSourceType instrumentType;
    private List<DataSourceType> instrumentTypes = new ArrayList<DataSourceType>();
    private List<Node> nodes = new ArrayList<Node>();
    private Node node;

    public InstrumentSettingsActionBean() {
        this.biz = new AsemoBizWrapper().getBusinessBean();
    }

    public Instrument getInstrument() {
        return instrument;
    }

    public void setInstrument(Instrument instrument) {
        this.instrument = instrument;
    }

    public List<DataSourceType> getInstrumentTypes() {
        return instrumentTypes;
    }

    public void setInstrumentTypes(List<DataSourceType> instrumentTypes) {
        this.instrumentTypes = instrumentTypes;
    }

    public DataSourceType getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(DataSourceType instrumentType) {
        this.instrumentType = instrumentType;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    @Before(stages = LifecycleStage.BindingAndValidation)
    public void rehydrate() {
        this.instrumentTypes = this.biz.getDataSourceTypes();
        this.nodes = this.biz.getAllNodes();
        String instrumentId = this.getContext().getRequest().getParameter("instrument.id");
        if (instrumentId != null && !instrumentId.isEmpty()) {
            this.instrument = this.biz.getInstrument(Integer.parseInt(instrumentId));
        }
    }

    public Resolution preEdit() {
        return new ForwardResolution(Urls.ADMIN_INSTRUMENT_SETTINGS);
    }

    public Resolution create() {
        try {
            if (this.node == null) {
                this.biz.createInstrument(this.instrument.getUuid(), this.instrumentType.getId());
            } else {
                this.biz.createInstrument(this.instrument.getUuid(), this.instrumentType.getId(), this.node.getId());
            }
            return new RedirectResolution(InstrumentsActionBean.class);
        } catch (UuidNotUniqueException ex) {
            ValidationErrors errors = new ValidationErrors();
            errors.add("instrument.uuid", new LocalizableError("uuidNotUnique"));
            getContext().setValidationErrors(errors);
            return this.getContext().getSourcePageResolution();
        }
    }

    public Resolution save() {
        this.biz.updateInstrument(this.instrument.getId(), (this.node != null) ? (Integer)this.node.getId() : null);
        return new RedirectResolution("/admin/Instruments.action");
    }

    public Resolution cancel() {
        return new RedirectResolution(InstrumentsActionBean.class);
    }

    @DefaultHandler
    public Resolution view() {
        return new ForwardResolution(Urls.ADMIN_INSTRUMENT_SETTINGS);
    }

}
