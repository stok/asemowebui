/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.web.ui;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.Initializer;
import fi.stok.asemo.biz.exception.PasswordException;
import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.biz.ejb.entity.schema.UserGroup;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Juha Loukkola
 */
@WebServlet(name = "StartupServlet", urlPatterns = {"/StartupServlet"})
public class StartupServlet extends HttpServlet {

    private static final String USER_GROUPS = "ADMIN,USER,DEMO";
    private static final String ADMIN_GROUP = "ADMIN";
    private static final String ADMIN_USER = "admin";
    private static final String ADMIN_PASSWORD = "salasana";
    @Resource(name = "AsemoInitializationSettings")
    private Properties initializationSettings;
    @EJB
    private Business biz;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        this.initialize();
        PrintWriter out = response.getWriter();
        try {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet StartupServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet StartupServlet at " + request.getContextPath() + "</h1>");
            out.println("Asemo has been initialized");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }
    
    void initialize() {

        // Setup user groups(roles)
        String str = initializationSettings.getProperty("userGroups", StartupServlet.USER_GROUPS);
        String[] roles = str.split("[,]");
        for (String role : roles) {
            if (this.biz.getUserGroup(role) == null) {
                UserGroup group = new UserGroup(role);
                this.biz.addUserGroup(group);
            }
        }

        // Setup admin user
        String adminUser = this.initializationSettings.getProperty("adminUser", StartupServlet.ADMIN_USER);
        String adminPassword = this.initializationSettings.getProperty("adminPassword", StartupServlet.ADMIN_PASSWORD);
        String adminGroup = this.initializationSettings.getProperty("adminGroup", StartupServlet.ADMIN_GROUP);
        // Check if admin user is already been created if not create a new
        User admin = this.biz.getUser(adminUser);
        if (admin == null) {
            try {
                admin = new User(adminUser, adminPassword);
                admin.getGroups().add(this.biz.getUserGroup(adminGroup));
                this.biz.saveUser(admin);
            } catch (PasswordException ex) {
                Logger.getLogger(Initializer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
