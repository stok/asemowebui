package fi.stok.asemo.web.ui.stripes.admin;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.exception.PasswordException;
import fi.stok.asemo.biz.ejb.entity.schema.Customer;
import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.biz.AsemoBizWrapper;
import fi.stok.asemo.web.ui.stripes.AsemoActionBean;
import java.util.List;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.controller.LifecycleStage;

/**
 *
 *
 * @author Juha Loukkola
 */
public class CustomerSettingsActionBean extends AsemoActionBean {

    private Customer customer;
    private User user;
    private String attachUserId;
    private List<User> availableUsers;
    private Business biz;

    public CustomerSettingsActionBean() {
        this.biz = new AsemoBizWrapper().getBusinessBean();
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAttachUserId() {
        return attachUserId;
    }

    public void setAttachUserId(String attachUserId) {
        this.attachUserId = attachUserId;
    }

    public List<User> getAvailableUsers() {
        return availableUsers;
    }

    public void setAvailableUsers(List<User> availableUsers) {
        this.availableUsers = availableUsers;
    }

    @Before(stages = LifecycleStage.BindingAndValidation)
    public void rehydrate() {
        String customerId = this.getContext().getRequest().getParameter("customer.id");
        if (customerId != null && !customerId.isEmpty()) {
            this.customer = this.biz.getCustomer(Integer.parseInt(customerId));
        }

        String userId = this.getContext().getRequest().getParameter("user.id");
        if (userId != null && !userId.isEmpty()) {
            this.user = this.biz.getUser(Integer.parseInt(userId));
        }
    }

    public Resolution save() throws PasswordException, ClassNotFoundException {
        this.customer.setUser(user);
        this.biz.updateCustomer(customer);
        return new RedirectResolution(CustomersActionBean.class);
    }

    public Resolution preEdit() {
        this.customer = this.biz.getCustomer(customer.getId());
        this.user = this.customer.getUser();
        this.PopulateDomainModel();
        if (user != null) {
            this.availableUsers.add(this.user);
        }
        return new ForwardResolution(Urls.ADMIN_CUSTOMER_SETTINGS);
    }

    @DefaultHandler
    public Resolution view() {
        this.PopulateDomainModel();
        return new ForwardResolution(Urls.ADMIN_CUSTOMER_SETTINGS);
    }

    public Resolution cancel() {
        return new RedirectResolution(CustomersActionBean.class);
    }

    private void PopulateDomainModel() {
        // Retrieve available users
        this.availableUsers = this.biz.getNonAttachedUsers();
    }
}
