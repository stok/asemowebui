package fi.stok.asemo.web.ui.stripes.admin;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.exception.PasswordException;
import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.biz.ejb.entity.schema.UserGroup;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.biz.AsemoBizWrapper;
import fi.stok.asemo.web.ui.stripes.AsemoActionBean;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.validation.SimpleError;
import net.sourceforge.stripes.validation.ValidationErrors;
import net.sourceforge.stripes.validation.ValidationMethod;

/**
 *
 * @author Juha Loukkola
 */
public class UserSettingsActionBean extends AsemoActionBean {

    private String password;
    private String confirmPassword;
    private User user;
    private Business biz;
    private List<UserGroup> groups;
    private List<String> assignedGroups = new ArrayList<String>();

    public UserSettingsActionBean() {
        this.biz = new AsemoBizWrapper().getBusinessBean();
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<UserGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<UserGroup> groups) {
        this.groups = groups;
    }

    public List<String> getAssignedGroups() {
        return assignedGroups;
    }

    public void setAssignedGroups(List<String> assignedGroups) {
        this.assignedGroups = assignedGroups;
    }

    @Before(stages = LifecycleStage.BindingAndValidation)
    public void rehydrate() {
        String userId = this.getContext().getRequest().getParameter("user.id");
        if (userId != null && !userId.isEmpty()) {
            this.user = this.biz.getUser(Integer.parseInt(userId));
        }  
    }

    public Resolution preEdit() {
        this.user = this.biz.getUser(user.getId());
        this.populateDomainModelForView();
        for (UserGroup ug : this.user.getGroups()) {
            this.assignedGroups.add(ug.getName());
        }
        return new ForwardResolution(Urls.ADMIN_USER_SETTINGS);
    }

    @DefaultHandler
    public Resolution view() {
        this.populateDomainModelForView();
        return new ForwardResolution(Urls.ADMIN_USER_SETTINGS);
    }

    public Resolution save() throws PasswordException {        
        if (this.password != null && this.password.length() > 0) {
            this.user.storePassword(password);
        }

        // Update userroles
        this.user.setGroups(new ArrayList<UserGroup>());
        for (String role : this.assignedGroups) {
            this.user.getGroups().add(this.biz.getUserGroup(role));
        }
        this.biz.updateUser(this.user);
        return new RedirectResolution(UsersActionBean.class);
    }

    public Resolution create() throws NoSuchAlgorithmException, PasswordException {
        this.user.storePassword(password);
        this.biz.saveUser(this.user);
        return new RedirectResolution(UsersActionBean.class);
    }

    public Resolution cancel() {
        return new RedirectResolution(UsersActionBean.class);
    }

    @ValidationMethod(on = {"save", "create"})
    public void validatePassword(ValidationErrors errors) {
        if ((this.password != null) && (this.confirmPassword != null)) {
            if (!this.password.equals(this.confirmPassword)) {
                errors.add("confirmPassword", new SimpleError("Passwords didn't match"));
            }
        }
    }

    private void populateDomainModelForView() {
        this.groups = this.biz.getUserGroups();
    }
}
