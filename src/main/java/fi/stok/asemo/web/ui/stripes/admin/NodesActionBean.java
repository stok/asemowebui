/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.web.ui.stripes.admin;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.ejb.entity.schema.Node;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.biz.AsemoBizWrapper;
import fi.stok.asemo.web.ui.stripes.AsemoActionBean;
import java.util.ArrayList;
import java.util.List;
import net.sourceforge.stripes.action.*;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.validation.Validate;

/**
 *
 * @author Juha Loukkola
 */
public class NodesActionBean extends AsemoActionBean {

    private Business biz;
    private List<Node> nodes = new ArrayList<Node>();
    @Validate(on = "remove", required = true)
    private int[] deleteIds;

    public NodesActionBean() {
        this.biz = new AsemoBizWrapper().getBusinessBean();
    }

    public int[] getDeleteIds() {
        return deleteIds;
    }

    public void setDeleteIds(int[] deleteIds) {
        this.deleteIds = deleteIds;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    @Before(stages = LifecycleStage.BindingAndValidation)
    public void rehydrate() {
        this.getContext().getRequest().setAttribute("urls", new Urls());
        this.nodes = this.biz.getAllNodes();
    }

    public Resolution create() {
        return new RedirectResolution(NodeSettingsActionBean.class);
    }

    public Resolution remove() {
        for (int id : this.deleteIds) {
            this.biz.deleteNode(id);
        }
        return new RedirectResolution(NodesActionBean.class);
    }

    @DefaultHandler
    public Resolution view() {
        return new ForwardResolution(Urls.ADMIN_NODES);
    }
}
