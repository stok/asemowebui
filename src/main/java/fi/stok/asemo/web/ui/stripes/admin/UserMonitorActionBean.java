/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.web.ui.stripes.admin;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.biz.AsemoBizWrapper;
import fi.stok.asemo.web.ui.stripes.AsemoActionBean;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;

/**
 *
 * @author Juha Loukkola
 */
public class UserMonitorActionBean extends AsemoActionBean {
    private Business biz = new AsemoBizWrapper().getBusinessBean();
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @DefaultHandler
    public Resolution login() throws ClassNotFoundException {
       // Set user to the session context
        this.user = this.biz.getUser(user.getId());
        getContext().setUser(user, "DEMO");
        return new RedirectResolution(Class.forName(Urls.USER_START_ACTION).asSubclass(AsemoActionBean.class));
    }
}
