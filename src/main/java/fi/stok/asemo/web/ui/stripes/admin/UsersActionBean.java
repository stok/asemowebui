/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.web.ui.stripes.admin;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.biz.AsemoBizWrapper;
import fi.stok.asemo.web.ui.stripes.AsemoActionBean;
import java.util.List;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.validation.Validate;

/**
 *
 * @author Juha Loukkola
 */
public class UsersActionBean extends AsemoActionBean {

    private Business biz;
    private List<User> users;
    @Validate(on = "remove", required = true)
    private int[] deleteIds;

    public UsersActionBean() {
        this.biz = new AsemoBizWrapper().getBusinessBean();
    }

    public int[] getDeleteIds() {
        return deleteIds;
    }

    public void setDeleteIds(int[] deleteIds) {
        this.deleteIds = deleteIds;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Before(stages = LifecycleStage.BindingAndValidation)
    public void rehydrate() {
        this.getContext().getRequest().setAttribute("urls", new Urls());
        this.users = this.biz.getUsers();
    }

    public Resolution create() {
        return new RedirectResolution(UserSettingsActionBean.class);
    }

    public Resolution remove() {
        for (int id : this.deleteIds) {
            User user = this.biz.getUser(id);
//            // Remove user from user group mappings
//            for (UserGroup ug : user.getGroups()) {
//                ug.getUsers().remove(user);
//            }
            // Remove binding to customer
            if (user.getCustomer() != null) {
                user.getCustomer().setUser(null);
            }
            // Remove user entity from DB
            this.biz.deleteUser(id);
        }
        return new RedirectResolution(UsersActionBean.class);
    }

    @DefaultHandler
    public Resolution view() {
        return new ForwardResolution(Urls.ADMIN_USERS);
    }
}
