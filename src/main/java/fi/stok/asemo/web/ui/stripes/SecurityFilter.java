package fi.stok.asemo.web.ui.stripes;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.biz.AsemoBizWrapper;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sourceforge.stripes.util.StringUtil;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import javax.ejb.EJB;

/**
 * A simplistic security filter for Asemo Web UI that ensures that the user is
 * logged in before allowing access to any secured pages.
 *
 * @author Juha Loukkola
 */
public class SecurityFilter implements Filter {

    private static final Set<String> publicUrls = new HashSet<String>();

    // White list of public URLs
    static {
        publicUrls.add(Urls.USER_LOGIN);
        //TODO: Get ridof hard coded urls
        publicUrls.add("/Login.action");
        publicUrls.add("/");
        publicUrls.add("/admin/Login.action");
        publicUrls.add(Urls.ADMIN_LOGIN);
        publicUrls.add(Urls.USER_REQUEST_PASSWORD);
        publicUrls.add("/RequestNewPassword.action");
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest,
            ServletResponse servletResponse,
            FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        User admin = (User) request.getSession().getAttribute("admin");
        User user = (User) request.getSession().getAttribute("user");
        String role = (String) request.getSession().getAttribute("role");

        // Allow everobody to access public resources
        if (isPublicResource(request)) {
            filterChain.doFilter(request, response);
        } // Restrict access to non-public admin resources
        else if (isAdminResource(request)) {
            if (admin != null) {
                filterChain.doFilter(request, response);
            } else {
                // Redirect the user to a admin login page, noting where they were coming from
                String targetUrl = StringUtil.urlEncode(request.getServletPath());
                response.sendRedirect(
                        request.getContextPath() + Urls.ADMIN_LOGIN + "?targetUrl=" + targetUrl);
            }
        } // Restrict access of an ordinary user and a demo user(can access a subset of functionality of an ordinary user)
        else if (isUserResource(request)) {
            if ("USER".equals(role)) {
                filterChain.doFilter(request, response);
            } else if ("DEMO".equals(role)) {
                if (isDemoResource(request)) {
                    filterChain.doFilter(request, response);
                } else {
                    String errorMessage = StringUtil.urlEncode("Tätä toimintoa ei voi käyttää demo-tilassa");
                    // Redirect to an error page: "User in a DEMO-role cannot access this resource"
                    response.sendRedirect(
                        request.getContextPath() + "/Error.jsp?description=" + errorMessage);
                }
            } else {
                // Redirect the user to the login page, noting where they were coming from
                String targetUrl = StringUtil.urlEncode(request.getServletPath());
                response.sendRedirect(
                        request.getContextPath() + "/index.jsp?targetUrl=" + targetUrl);
            }
        }
    }

    /**
     * Method that checks the request to see if it is for a publicly accessible
     * resource
     */
    protected boolean isPublicResource(HttpServletRequest request) {
        String resource = request.getServletPath();
        return publicUrls.contains(resource)
                || (!resource.endsWith(".jsp") && !resource.endsWith(".action"));
    }

    /**
     * Method that checks the request to see if it is for an admin accessible
     * resource that is a resources which have Urls.ADMIN_CONTENT_ROOT as its
     * prefix
     */
    protected boolean isAdminResource(HttpServletRequest request) {
        String resource = request.getServletPath();
        return resource.startsWith(Urls.ADMIN_CONTENT_ROOT);
    }

    protected boolean isUserResource(HttpServletRequest request) {
        return !isAdminResource(request) && !isPublicResource(request);
    }

    // Restrict access to submit 
    //  * password change
    //  * user settings change
    //  * residence settings change
    protected boolean isDemoResource(HttpServletRequest request) {
        String save = request.getParameter("save");
        return save == null && isUserResource(request);
    }

    /**
     * Does nothing.
     */
    @Override
    public void destroy() {
    }
}
