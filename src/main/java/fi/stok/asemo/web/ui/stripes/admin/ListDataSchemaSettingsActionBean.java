/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.web.ui.stripes.admin;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.ejb.entity.schema.ListDataSchema;
import fi.stok.asemo.biz.ejb.entity.schema.Value;
import fi.stok.asemo.biz.ejb.entity.schema.VariableType;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.biz.AsemoBizWrapper;
import fi.stok.asemo.web.ui.stripes.AsemoActionBean;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.controller.LifecycleStage;

/**
 *
 * @author Juha Loukkola
 */
public class ListDataSchemaSettingsActionBean extends AsemoActionBean {

    private static Business biz = new AsemoBizWrapper().getBusinessBean();
    private ListDataSchema schema;
    private List<VariableType> elements = new ArrayList();
    private List<VariableType> deletedElements = new ArrayList();
    private VariableType newElement;
    private static final List<String> dataTypes = Value.subClasses;
    private int[] deleteIds;

    public ListDataSchema getSchema() {
        return schema;
    }

    public void setSchema(ListDataSchema schema) {
        this.schema = schema;
    }

    public List<VariableType> getElements() {
        return elements;
    }

    public void setElements(List<VariableType> elements) {
        this.elements = elements;
    }

    public VariableType getNewElement() {
        return newElement;
    }

    public void setNewElement(VariableType newElement) {
        this.newElement = newElement;
    }

    public int[] getDeleteIds() {
        return deleteIds;
    }

    public void setDeleteIds(int[] deleteIds) {
        this.deleteIds = deleteIds;
    }

    public List<String> getDataTypes() {
        return dataTypes;
    }

    @Before(stages = LifecycleStage.BindingAndValidation)
    public void rehydrate() {
        this.getContext().getRequest().setAttribute("urls", new Urls());
    }

    public Resolution preEdit() {
        this.schema = (ListDataSchema) biz.getDataSchema(schema.getId());
        this.elements = this.schema.getElements();
        return new RedirectResolution(Urls.ADMIN_LIST_DATA_SCHEMA_SETTINGS).flash(this);
    }

    public Resolution addElement() {
        this.elements.add(newElement);
        return new RedirectResolution(Urls.ADMIN_LIST_DATA_SCHEMA_SETTINGS).flash(this);
    }

    public Resolution deleteElements() {
        Iterator<VariableType> iterator = this.elements.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            VariableType element = iterator.next();
            for (int id : this.getDeleteIds()) {
                if (id == i) {
                    this.deletedElements.add(element);
                    iterator.remove();
                    break;
                }
            }
            i++;
        }
        return new RedirectResolution(Urls.ADMIN_LIST_DATA_SCHEMA_SETTINGS).flash(this);
    }
    
    public Resolution create() {
        biz.createDataSchema(this.schema.getName(), this.schema.getDescription(), this.elements);
        return new RedirectResolution(DataSchemasActionBean.class);
    }

    // TODO: Validation
    public Resolution save() {
        biz.updateDataSchema(this.schema.getId(), this.schema.getName(), this.schema.getDescription(), this.elements);
        return new RedirectResolution(DataSchemasActionBean.class);
    }

    public Resolution cancel() {
        return new RedirectResolution(DataSchemasActionBean.class);
    }

    @DefaultHandler
    public Resolution view() {
        return new ForwardResolution(Urls.ADMIN_LIST_DATA_SCHEMA_SETTINGS);
    }
}
