/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.web.ui.stripes;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.exception.PasswordException;
import fi.stok.asemo.biz.ejb.entity.schema.User;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.biz.AsemoBizWrapper;
import java.security.NoSuchAlgorithmException;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.validation.SimpleError;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationErrors;
import net.sourceforge.stripes.validation.ValidationMethod;

/**
 *
 * @author Juha Loukkola
 */
public class ChangePasswordActionBean extends AsemoActionBean {

    //private User user;
    private Integer id;
    private String password;
    private String confirmPassword;
    private AsemoBizWrapper bizWrapper;
    private Business biz;

    public ChangePasswordActionBean() {
        this.bizWrapper = new AsemoBizWrapper();
        this.biz = this.bizWrapper.getBusinessBean();
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }
    
    @Validate(required=true, on="save")
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getPassword() {
        return password;
    }

    @Validate(required=true, on="save")
    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


//    public User getUser() {
//        return user;
//    }
//
//    public void setUser(User user) {
//        this.user = user;
//    }
    
    @DefaultHandler
    public Resolution view() {
        return new ForwardResolution(Urls.USER_CHANGE_PASSWORD);
    }

    public Resolution save() throws PasswordException {
        User user = this.biz.getUser(this.getId());
        user.storePassword(password);
        this.biz.saveUser(user);
        return new RedirectResolution(ResidenceSettingsActionBean.class);
    }

    @ValidationMethod(on = "save")
    public void validatePassword(ValidationErrors errors) throws NoSuchAlgorithmException {
        if ((this.password != null) && (this.confirmPassword != null)) {
            if (!this.password.equals(this.confirmPassword)) {
                errors.add("confirmPassword", new SimpleError("Salasanat eivät täsmää"));
            }
        }
    }
}
