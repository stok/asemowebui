/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.stok.asemo.web.ui.biz;

import fi.stok.asemo.biz.Business;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author Juha Loukkola
 */
public class AsemoBizWrapper {

    private Business businessBean;

    public AsemoBizWrapper() {
        this.businessBean = this.lookupBusinessBeanLocal();
    }

    public Business getBusinessBean() {
        return businessBean;
    }

    public void setBusinessBean(Business business) {
        this.businessBean = business;
    }

    private Business lookupBusinessBeanLocal() {
        try {
            Context c = new InitialContext();
            return (Business) c.lookup("java:app/AsemoBusiness/BusinessBean!fi.stok.asemo.biz.Business");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}