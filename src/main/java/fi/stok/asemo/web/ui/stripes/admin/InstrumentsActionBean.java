package fi.stok.asemo.web.ui.stripes.admin;

import fi.stok.asemo.biz.Business;
import fi.stok.asemo.biz.ejb.entity.schema.Instrument;
import fi.stok.asemo.web.ui.Urls;
import fi.stok.asemo.web.ui.biz.AsemoBizWrapper;
import fi.stok.asemo.web.ui.stripes.AsemoActionBean;
import java.util.List;
import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.validation.Validate;

/**
 *
 * @author Juha Loukkola
 */
public class InstrumentsActionBean extends AsemoActionBean {

    private Business biz;
    private List<Instrument> instruments;
    @Validate(on = "remove", required = true)
    private int[] deleteIds;

    public InstrumentsActionBean() {
        this.biz = new AsemoBizWrapper().getBusinessBean();
    }

    public int[] getDeleteIds() {
        return deleteIds;
    }

    public void setDeleteIds(int[] deleteIds) {
        this.deleteIds = deleteIds;
    }

    public List<Instrument> getInstruments() {
        return instruments;
    }

    @Before(stages = LifecycleStage.BindingAndValidation)
    public void rehydrate() {
        this.getContext().getRequest().setAttribute("urls", new Urls());
        this.instruments = this.biz.getInstruments();
    }

    public Resolution remove() {
        for (int id : this.deleteIds) {
            this.biz.deleteInstrument(id);
        }
        return new RedirectResolution(InstrumentsActionBean.class);
    }

    public Resolution create() {
        return new RedirectResolution(InstrumentSettingsActionBean.class);
    }

    @DefaultHandler
    public Resolution view() {
        return new ForwardResolution(Urls.ADMIN_INSTRUMENTS);
    }
}
