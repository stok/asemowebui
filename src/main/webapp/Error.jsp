<%-- 
    Document   : Error.jsp
    Created on : May 23, 2012, 11:08:39 AM
    Author     : juha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>
<jsp:useBean id="urls" class="fi.stok.asemo.web.ui.Urls" type="fi.stok.asemo.web.ui.Urls" scope="page"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>ASEMO - ${title}</title>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/reset.css"/>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/text.css"/>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/grid.css"/>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/content.css"/>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/layout.css"/>
    </head>

    <body>
        <div id="user_header" class="grid_full">
            <div>
                <a class="logo" href="/"><strong id="logo_text">asemo.fi</strong></a>
            </div><br/>
        </div>

        <div id="site-info">
            <div id="footer">
                <div id="footer_content">
                    <h1>${param.description}</h1>
                </div>
            </div>
        </div>

    </body>
</html>
