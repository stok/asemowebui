<%-- 
    Document   : index
    Created on : Dec 19, 2011, 8:36:01 AM
    Author     : juha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>
<jsp:useBean id="urls" class="fi.stok.asemo.web.ui.Urls" type="fi.stok.asemo.web.ui.Urls" scope="page"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head profile="http://www.w3.org/2005/10/profile">
        <link rel="icon"
              type="image/vnd.microsoft.icon"
              href="${ctx}/favicon.ico" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>ASEMO - ${title}</title>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/reset.css"/>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/text.css"/>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/grid.css"/>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/content.css"/>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/layout.css"/>
    </head>

    <body class="fp">
        <div id="container">

            <div id="frontpage_header">
                <div id="frontpage_logo">
                    <!--                    <a href="/">-->
                    <img alt="asemo.fi" src="${ctx}/img/asemo_logo_fp.png">
                        <!--                    </a>-->
                </div>

                <div id="navigation"> 
                    <stripes:form beanclass="${urls.USER_LOGIN_ACTION}" focus="">
                        <table class="fp text_default">
                            <tr>
                                <th><stripes:label for="Käyttäjätunnus"/></th>
                                <th><stripes:label for="Salasana"/></th>                    
                            </tr>
                            <tr>
                                <td><stripes:text name="username" size="12" value="${user.username}" class="text_input_blue"/></td>
                                <td><stripes:password name="password" size="12" class="text_input_blue"/></td>
                                <td style="text-align: center;">
                                    <%-- If the security servlet attached a targetUrl, carry that along. --%>
                                    <stripes:hidden name="targetUrl" value="${request.parameterMap['targetUrl']}"/>
                                    <stripes:submit name="login" value="Kirjaudu sisään" class="button"/>
                                </td>
                            </tr>
                        </table>
                        <%-- <ul class="fp">
                             <li>
                                 <stripes:label for="Käyttäjätunnus"/>
                                 <br/>
                                 <stripes:text name="username" value="${user.username}"/>
                             </li>
                             <li>
                                 <stripes:label for="Salasana"/>
                                 <br/>
                                 <stripes:password name="password"/>
                             </li>
                             <li>
                                 <br/>
                                 <stripes:hidden name="targetUrl" value="${request.parameterMap['targetUrl']}"/>
                                 <stripes:submit name="login" value="Kirjaudu sisään"/>
                             </li>
                         </ul> --%>
                    </stripes:form>
                </div>
            </div> 

            <div id="content">
                <div id="site-info">
                    <ul>
                        <li><stripes:errors beanclass="${urls.USER_LOGIN_ACTION}"/></li>
                        <li>
                            <stripes:link beanclass="${urls.USER_REQUEST_PASSWORD_ACTION}">
                                <h1><span style="color:#009edf;">&raquo;</span>&nbsp;Unohditko salasanasi?</h1>
                            </stripes:link>
                        </li>
                        <li>
                            <stripes:link beanclass="${urls.USER_LOGIN_ACTION}" event="demoLogin" addSourcePage="true">
                                <h1><span style="color:#009edf;">&raquo;</span>&nbsp;Kokeile palvelua ilman käyttäjätunnuksia.</h1>
                                <stripes:param name="username" value="LivingLab1"/>
                            </stripes:link>
                        </li>
                        <li>
                            <stripes:link href="http://www.posintra.fi/wp-content/uploads/2013/06/Asennusohjeet-currentcost-optinen.pdf">
                                <h1><span style="color:#009edf;">&raquo;</span>&nbsp;Näin otat Asemon käyttöön kotonasi.</h1>
                            </stripes:link>
                        </li>
                        <!--  <li><a href="#" target="_self"><span style="color:#009edf;">&raquo;</span>&nbsp;lyhyt kuvaus järjestelmästä (yksi lause) och samma på svenska</a></li> -->
                    </ul>
                </div>
            </div>    

        </div>

        <div id="footer">
            <div>
                <a href="http://www.posintra.fi/stok/living-labs/">
                    <img alt="Sähköisen talotekniikan osaamis- ja kehittämiskeskus" src="${ctx}/img/stok-small.png">
                </a>
                <a href="http://www.posintra.fi/">
                    <img alt="Posintra Oy" src="${ctx}/img/posintra-small.png">
                </a>
                <a href="http://www.posintra.fi/stok/stok-energy-living-lab-rekisteriseloste/">
                    Rekisteriseloste
                </a>
            </div>
        </div>

    </body>
</html>

