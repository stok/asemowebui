/**
 * This script demonstrates normalizing data in the front end, and 
 * adding annotations to graph. 
 * 
 * Request this script with parameters 
 * 
 * int meter: id of the instrument in db
 * int from: timestamp of start time
 * int to: timestamp of end time
 * 
 * Improvements needed for Webkeruu functionality
 * 
 * This script load data from a start date to an end date, but does not 
 * have a good data structure for dynamically asking for more data of a 
 * particular region of the chart. One untested solution could be an 
 * associative array which contains the cached data as: 
 * 
 * {
 *   day[] {
 *     hour[] {
 *       data_points[],
 *       minute[] {
 *         data_points[],
 *         raw { data_points[] }
 *       }
 *     }
 *   }
 * }
 * 
 * When loading data to the array, request normalized data from the 
 * backend, for the required resolution, which is calculated in the 
 * frontend. 
 * 
 * Speed: Normalization in the frontend is fairly fast, but when working 
 * with huge amounts of data it has its limitations. On a fast desktop 
 * computer, normalizing 230000 points of raw data into day, hour, min 
 * takes about 500ms, which is OK if it is done once, but not if it is 
 * done every time data is loaded, as is done here.
 * 
 */
        

"use strict";
var reductionInterval = 151;
// data points loaded from server
var stat_data_points = 0;
var status_string = "Loading data";
var data_delay = "n/a";

// Executed on window resize, and is for fullscreen use
function resize() {
    var vertical_offset = 85;
    var height = ($(window).height()) - vertical_offset;
    if ( height < min_height ) {
        height = min_height;
    }
    $("div#graph").attr('style', 'width: 1000px; height: 150px;');
    if (dygraph!=null) {
        dygraph.resize();
    }

    //$("div#info").html("Graph width: "+document.getElementById("graph").offsetWidth+" px<br />");
    //$("div#info").prepend("Graph height: "+document.getElementById("graph").offsetHeight+" px<br />");
}
            
// Used by graph for calculating axis value
function round(num, places) {
    var shift = Math.pow(10, places);
    return Math.round(num * shift)/shift;
};
            
var suffixes = ['W', 'kW', 'MW', 'GW', 'TW'];
function formatValue(v) {
    if ( v == 0 ) {return "0W"; }
    var magnitude = Math.floor((Math.log(v+0.1)/Math.LN10)/3);
    if ( v < 1000 ) {
        return Math.round(v) + suffixes[0];
    }
    if (magnitude > suffixes.length - 1)
        magnitude = suffixes.length - 1;
    //return Math.round((v)/Math.pow(10, magnitude*3)).toPrecision(2) + "("+magnitude+")";
    return (v / Math.pow(10, magnitude * 3)).toPrecision(2) + suffixes[magnitude];
}
            
// Used for additional info/debug
function updateInfo() {
    $("div#info").html(status_string+"<br/>Data delay "+timePeriodToReadable(data_delay)+" sec<br/>"+stat_data_points+" data point loaded altogether<br/>last timestamp: "+last_ts+ "<br>Items in datastore: "+ data_store.length );
    updateGraph();
}
            
// creates string from time, used for displaying 
function timePeriodToReadable(time) {
    var str = "";
    str += (Math.floor(time/(3600*24))>0)?Math.floor(time/(3600*24))+"d ":"";
    time -= Math.floor(time/(3600*24))*3600*24;
    str += (Math.floor(time/(3600))>0)?Math.floor(time/3600)+"h ":"";
    time -= Math.floor(time/(3600))*3600;
    str += (Math.floor(time/(60))>0)?Math.floor(time/60)+"m ":"";
    time -= Math.floor(time/(60))*60;
    str += (time>0)?Math.floor(time)+"s ":"";
    return str;
}

// timestamp in milliseconds
function wholeMin(timestamp) {
    return Math.floor(timestamp/300000)*300000;
}
            
// timestamp in milliseconds
function wholeHour(timestamp) {
    return Math.floor(timestamp/3600000)*3600000;
}
            
// timestamp in milliseconds
function wholeDay(timestamp) {
    var ts = new Date(timestamp);
    ts.setHours(0, 0, 0, 0);
    var date = new Date(ts.getTime());
    return date.getTime();
}
            
function setDataSource(src) {
    dataSource = src;
    updateGraph();
}

function showNowBackTo(windowSize) {
    if ( dygraph != null ) {
        //var curWindow = dygraph.xAxisRange();
        var newWindow = [new Date().getTime()-windowSize, new Date().getTime()];
        dygraph.updateOptions( { dateWindow: newWindow } );
    }
}

/**
 * This function does two things, implemented as such for speed.
 * 
 * It analyzes the data for periods using a fixed maximum interval. 
 * If a greater gap is found in the data it will mark it as a 
 * period.
 * 
 * Secondly, it normalizes data to 3 different intervalls, i.e. 
 * integrates numerically and stores integrations to 5 minutes, 
 * hours and days. The result is stored by preceeding timestamp in 
 * an associative array, i.e. timestamp for the 5. november is 
 * 1320451200, the integrated value for the whole day is stored in 
 * norm_day[1320451200].
 * 
 * Recognize that the timestamp used by JavaScript is in milli-
 * seconds, and the factor if 1000 used is converting between ms and
 * seconds.
 * 
 */

function analyzeDataPeriods() {
    var periodList = [];
                
    var period_d_max = 30000; // split period after bigger gap in milliseconds than this
                
    var first = 0; // first point in period
    var last = 0; // Last point
    var d_max = 0; // delta max, longest interval in period
    var d_min = 9999; // delta min, shortest interval in period

    norm_min = {};
    norm_hour = {};
    norm_day = {};
    //norm_hour.push("test");

    // we need at least two points of data, otherwise return empty period list
    if ( data_store.length < 2 ) {
        return [];
    }

    var lastWholeMinBegin = wholeMin(data_store[0][0]);
    var lastWholeMinEnd = lastWholeMinBegin+300000; // 300000 ms/5min

    var lastWholeHourBegin = wholeHour(data_store[0][0]);
    var lastWholeHourEnd = lastWholeHourBegin+3600000; // 3600000 ms/h
                
    var lastWholeDayBegin = wholeDay(data_store[0][0]);
    var lastWholeDayEnd = lastWholeDayBegin+3600000*24; // 3600000*24 ms/h

    for (var i = 0; i < data_store.length; i++) {
        if (first ===0 ) {
            first = data_store[i][0];
            last = data_store[i][0];
            continue;
        }
        // check if there is a gap since last datapoint, if so add period
        if ( data_store[i][0] - last > period_d_max ) {
            periodList.push([first, last, d_min, d_max, (data_store[i][0] - last)]);
            first = data_store[i][0];
            d_max = 0;
            d_min = 9999;
        } else { // if continous data update stats
            var wattHours = (data_store[i-1][1] * ((data_store[i][0]-data_store[i-1][0])/1000));
                        
            // Normalizing hourly data
            if ( typeof norm_min[lastWholeMinBegin] === "undefined" ) {
                norm_min[lastWholeMinBegin] = wattHours;
            } else {
                norm_min[lastWholeMinBegin] = norm_min[lastWholeMinBegin] + wattHours;
            }
            // Normalizing hourly data
            if ( typeof norm_hour[lastWholeHourBegin] === "undefined" ) {
                norm_hour[lastWholeHourBegin] = wattHours;
            } else {
                norm_hour[lastWholeHourBegin] = norm_hour[lastWholeHourBegin] + wattHours;
            }

            // Normalizing daily data
            if ( typeof norm_day[lastWholeDayBegin] === "undefined" ) {
                norm_day[lastWholeDayBegin] = wattHours;
            } else {
                norm_day[lastWholeDayBegin] = norm_day[lastWholeDayBegin] + wattHours;
            }
                        
            if ( data_store[i][0] - last > d_max ) {
                d_max = data_store[i][0] - last;
            }
            if ( data_store[i][0] - last < d_min ) {
                d_min = data_store[i][0] - last;
            }
            // check if we are within the normalization window, or move window forward
            if ( data_store[i][0] >= lastWholeMinEnd ) {
                lastWholeMinBegin = wholeMin(data_store[i][0]);
                lastWholeMinEnd = lastWholeMinBegin+300000; // 300000 ms/5min
            }
            // check if we are within the normalization window, or move window forward
            if ( data_store[i][0] >= lastWholeHourEnd ) {
                lastWholeHourBegin = wholeHour(data_store[i][0]);
                lastWholeHourEnd = lastWholeHourBegin+3600000; // 3600000 ms/h
            }
            // check if we are within the normalization window, or move window forward
            if ( data_store[i][0] >= lastWholeDayEnd ) {
                lastWholeDayBegin = wholeDay(data_store[i][0]);
                lastWholeDayEnd = lastWholeDayBegin+3600000*24; // 3600000*24 ms/h
            }
        }

        last = data_store[i][0];
    }
                
    periodList.push([first, data_store[data_store.length-1][0], d_min, d_max, (data_store[data_store.length-1][0]-last)]);

    var pStr = "";
                
    pStr = "Periods: "+periodList.length+"\n\n";
    for (var i = 0; i < periodList.length; i++) {
        pStr += "Period "+(i+1)+": "+periodList[i][0]+" to "+periodList[i][1]+"(len: "+timePeriodToReadable((periodList[i][1]-periodList[i][0])/1000)+"d_min="+(periodList[i][2]/1000)+", d_max="+(periodList[i][3]/1000)+", data_gap="+timePeriodToReadable(periodList[i][4]/1000)+")\n";
    }
                
    //alert(pStr);
                
                
    return periodList
}
            
function createGraph() {
    dygraph = new Dygraph(document.getElementById("graph"),
    [[new Date().getTime(), 55], [new Date().getTime()+5*60*1000, 33], [new Date()+10*60*1000, 76]],
    {
        labels: [ "Time", "Consumption", "None" ],
        fillGraph: true,
        //rollPeriod: 50,
        stepPlot: true,
        dateWindow: [1300293362000, 1300529375000],
        drawCallback: function(me, initial) {
            $("output#zoom").prepend("Zoomed/Draw: "+me.xAxisRange()+"<br />");
            zoomDataCheck(me.xAxisRange());
        },
        zoomCallback: function(minDate, maxDate, yRange) {
            $("output#zoom").prepend("Zoomed: "+minDate+", "+maxDate+"<br />");
            zoomDataCheck([minDate, maxDate]);
        },
        labelsDiv: document.getElementById("labels"),
        labelsDivWidth: 350,
        labelsDivStyles: {
            //'width': '350px',
            //'position': 'top right',
            'font-size': '0.9em'
        },
        yValueFormatter: formatValue,
        yAxisLabelFormatter: formatValue,
        includeZero: true
    });
    dygraph.updateOptions({
        pointClickCallback: function(event, p) {
            // Check if the point is already annotated.
            if (p.annotation) return;
                            
            // If not, add one.
            var ann = {
                series: p.name,
                xval: p.xval,
                shortText: "A",
                text: "Laitettiin uusi saunakiuas paikoilleen."
            };
            var anns = dygraph.annotations();
            anns.push(ann);
            dygraph.setAnnotations(anns);
        }});
}
            
function zoomDataCheck(xRange) {
    var min = xRange[0];
    var max = xRange[1];
    var scaleStr = "h";
    var scale = (2500*500000)/(max-min);
                
    if ( scale > 160 ) {
        scaleStr = 'r';
    } else if ( scale > 20 ) {
        scaleStr = 'm';
    } else if ( scale > 1 ) {
        scaleStr = 'h';
    } else {
        scaleStr = 'd';
    }
    if (dataSource!==scaleStr) {
        $("output#zoom").prepend("Zoom data source: "+scaleStr+"<br />");
        setDataSource(scaleStr);
    }
}
            
/**
 * This function checks for changes in data and zoom and finds the 
 * data points needed for drawing the current view. Dynamic loading
 * of data should be done from here.
 */            
function updateGraph() {
    var localarray = [];

    if ( dygraph === null ) {
        return 
    }

    // Check if data has changed
    if ( lastDataStoreRev === data_store_rev ) {
        // dont update if last update was called with same xRange
        if ( dygraph.xAxisRange()[0] === lastUpdateXRangeStart && dygraph.xAxisRange()[1] === lastUpdateXRangeEnd ) {
            return; 
        }
    }

    if ( dataSource === 'm') {
        $.each(norm_min, function(index, value) {
            if ( (index >= dygraph.xAxisRange()[0]-300000) && (index <= dygraph.xAxisRange()[1]+300000) ) {
                localarray.push([new Date(index*1), (value/(300))]);
            }
        });
    } else if ( dataSource === 'h') {
        $.each(norm_hour, function(index, value) {
            if ( (parseInt(index) >= dygraph.xAxisRange()[0]-3600000) && (index <= dygraph.xAxisRange()[1]+3600000) ) {
                localarray.push([new Date(index*1), (value/(3600))]);
            }
        });
    } else if ( dataSource === 'd') {
        $.each(norm_day, function(index, value) {
            if ( (index >= dygraph.xAxisRange()[0]-3600000*24) && (index <= dygraph.xAxisRange()[1]+3600000*24) ) {
                localarray.push([new Date(index*1), (value/(3600*24))]);
            }
        });
    } else if ( dataSource === 'r') {
        $.each(data_store, function(index, value) {
            if ( value[0] >= dygraph.xAxisRange()[0]-30000 && value[0] <= dygraph.xAxisRange()[1]+30000 ) {
                localarray.push([new Date(value[0]), value[1]]);
            }
        });
    } else {
        alert("no data source");
    }

    if ( localarray.length < 2 ) {
        $("output#result").prepend("Not enough points for graph. "+localarray.length+"<br />");
        $("output#result").prepend("Window("+dygraph.xAxisRange()[0]+", "+dygraph.xAxisRange()[1]+"<br />");
        return;
    }

    if ( dygraph != null && localarray.length>1 ) {
        var curWindow = dygraph.xAxisRange();
        lastUpdateXRangeStart = curWindow[0];
        lastUpdateXRangeEnd = curWindow[1];
        lastDataStoreRev = data_store_rev;
        $("output#stats").prepend("Points in graph: "+localarray.length+"<br />");
        dygraph.updateOptions( { 'file': localarray, dateWindow: curWindow } );
    }
}
            
function zoomIn() {
    if ( dygraph != null ) {
        var curWindow = dygraph.xAxisRange();
        var delta = curWindow[1] - curWindow[0];
        var newWindow = [parseInt(curWindow[0]+delta*0.2), parseInt(curWindow[1]-delta*0.2)];
        dygraph.updateOptions( { dateWindow: newWindow } );
        updateGraph();
    }
}
            
function zoomOut() {
    if ( dygraph != null ) {
        var curWindow = dygraph.xAxisRange();
        var delta = curWindow[1] - curWindow[0];
        var newWindow = [parseInt(curWindow[0]-delta*0.2), parseInt(curWindow[1]+delta*0.2)];
        dygraph.updateOptions( { dateWindow: newWindow } );
        updateGraph();
    }
}
            
function graphAndGo() {
    if ( working ) {
        return;
    } else {
        working = true;
    }
    var query = "/index.php";
    var meter = "1";
    var res = "raw";
    var cb_time_start = null;
    var cb_time_end = null;
    var tot_time_ms = null;

    $.ajax({
        type: "POST",
        url: '/index.php',
        data: { f: "getDyGraphPublic", meter: meter, from: last_ts, resolution: res },
        success: function(d2){
            cb_time_start = new Date().getTime();

            if ( !d2["points"] ) {
                return;
            }
            var last = d2["points"].length-1;
            if (last <= 0) {
                if ( data.length == 0 ) {
                    status_string = "No new data."
                } else {
                    //$("div#info").prepend("Data Loaded. <br/>last timestamp: "+last_ts+"<br />points loaded: "+d2["points"].length+"<br />Localtime: "+Math.round((new Date()).getTime()/1000)+"<br />");
                }
                clearInterval(interval);
                interval = setInterval(graphAndGo, 8000+Math.random()*4000);
            } else {
                last_ts = (Math.round(new Date(d2["points"][last][0]).getTime()/1000));
                // calculate the estimated delay from the instrument to the graph
                data_delay = (d2['server_time']-last_ts);
                //$("div#info").prepend("Data Loaded. <br/>last timestamp: "+last_ts+"<br />points loaded: "+d2["points"].length+"<br />Localtime: "+Math.round((new Date()).getTime()/1000)+"<br />Server time: "+d2['server_time']+"<br />");
                for ( var x in d2["points"] ) {
                    stat_data_points += 1;

                    var x_time = parseInt(d2["points"][x][0]);
                    var x_point = parseInt(d2["points"][x][1]);
                                
                    data_store.push([x_time, x_point]);
                }
                            
                data_log.push("  Now "+data_store.length+" items in store");
                data_store_rev++;
                            
                analyzeDataPeriods();
                updateGraph();

            }
            cb_time_end = new Date().getTime();

            $("div#info").prepend(" (tot time: "+(cb_time_end-cb_time_start)+"ms)");

            tot_time_ms = cb_time_end-cb_time_start;

            if ( tot_time_ms > 1000 ) {
                $("div#error").html("Extremely slow, stop loading! "+tot_time_ms+" ms<br />");
                $("div#error").slideDown(200);
                //reductionInterval-=30;
            } else if ( tot_time_ms > 500 ) {
                $("div#error").html("Extremely slow, still trying... "+tot_time_ms+" ms<br />");
                $("div#error").slideDown(200);
                //reductionInterval-=20;
            } else if ( tot_time_ms > 250 ) {
                $("div#error").html("Very slow "+tot_time_ms+" ms<br />");
                $("div#error").slideDown(200);
            } else if ( tot_time_ms > 50 ) {
                $("div#error").html("Slow "+tot_time_ms+" ms<br />");
                $("div#error").slideDown(200);
            } else if ( tot_time_ms > 10 ) {
                $("div#error").html("Stable "+tot_time_ms+" ms<br />");
                $("div#error").delay(5000).slideUp(500);
            } else {
                $("div#error").html("Fast "+reductionInterval+"<br />");
                $("div#error").delay(5000).slideUp(500);
                reductionInterval+=5;
            }

            working = false;
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert("error "+jqXHR.status+": \n\n"+jqXHR.responseText);
            var entry = document.createElement('div');
            entry.className += " error";
            entry.className += " 501";
            entry.innerHTML = jqXHR.responseText;
            $("div#error").prepend(entry);
        }
    });
}


$("form#presentation-view select").change(function (evt) {
    $.ajax({
        type: "GET",
        url: '/index.php?'+this.getAttribute('value'),
        data: $(this).serialize(),
        success: function(e){
            alert("Got the thing!");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert("error "+jqXHR.status+": \n\n"+jqXHR.responseText);
        }
    });
    return false;
});

function testWorker() {
    var d = new DataAnalyzer();
    d.test();
}


var data = new Array();
//    <?php
//if (isset($_REQUEST['from']) && isset($_REQUEST['to'])) {
//    echo 'var last_ts = ""+'.$_REQUEST['from']."\n";
//} else {
//    echo 'var last_ts = ""+(Math.round((new Date()).getTime()/1000)-(3600*';
//    echo (isset($_REQUEST['hrs']) ? $_REQUEST['hrs'] : '24');
//    echo '));'."\n";
//}
//    ?>
var min_height = 40;
var first = true;
var interval = null;
var interval_update = null;
var working = false;
var show_details = false;
var data_store = [];
var data_store_rev = 0; // revision of the data store, to identify changes in data.
var data_log = [];
var lastAnalyzedIndex = 0;
var norm_min = {}
var norm_hour = {};
var norm_day = {};
var dataSource = 'h';
var dygraph = null;
            
var lastUpdateXRangeStart = 0;
var lastUpdateXRangeEnd = 0;
var lastDataStoreRev = 0;

$(window).ready(graphAndGo);
$(window).ready(resize);
$(window).ready(createGraph);
$(window).ready(testWorker);
$(window).ready(function () {$("input#show-details").change();});
$(window).resize(resize);
$("input#show-details").change(function(evt) {
    show_details = evt.target.checked;
    if ( !show_details ) {
        $("div#info").slideUp(200);
    } else {
        $("div#info").slideDown(200);
    }
});
//$("a#enlarge").click(resize);
var interval = setInterval(graphAndGo, 50);
var interval_update = setInterval(updateInfo, 500);

