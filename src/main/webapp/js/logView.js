/**
 * @author Juha Loukkola
 * LogView abstarcts an graph element as view.
 * 
 */

/**
 * Work around for adding bind suport for older browsers. 
 * See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind
 */
if (!Function.prototype.bind) {
    Function.prototype.bind = function(oThis) {
        if (typeof this !== "function") {
            // closest thing possible to the ECMAScript 5 internal IsCallable function
            throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
        }

        var aArgs = Array.prototype.slice.call(arguments, 1),
                fToBind = this,
                fNOP = function() {
        },
                fBound = function() {
            return fToBind.apply(this instanceof fNOP && oThis
                    ? this
                    : oThis,
                    aArgs.concat(Array.prototype.slice.call(arguments)));
        };

        fNOP.prototype = this.prototype;
        fBound.prototype = new fNOP();

        return fBound;
    };
}

/**
 * 
 * @param {type} container
 * @param {type} stream
 * @param {type} resolution
 * @param {type} from
 * @param {type} to
 * @param {type} url
 * @param {type} xLabel
 * @param {type} yLabel
 * @param {type} unit
 * @returns {LogView}
 */
function LogView(container, stream, resolution, from, to, url, xLabel, yLabel, unit) {

    //************************************************************************
    // DECLARATIONS OF PUBLIC PROPERTIES
    //************************************************************************
    this.container = container;
    this.stream = stream;
    this.resolution = resolution;
    this.from = from;
    this.to = to;
    this.dataSourceURL = url;
    this.log = [];
    this.xLabel = xLabel;
    this.yLabel = yLabel;
    this.unit = unit;
    this.graphInstance = null;

//************************************************************************
// DECLARATIONS OF PRIVATE METHODS
//************************************************************************

//************************************************************************
// DECLARATIONS OF PUBLIC PRIVILEGED METHODS
//************************************************************************    


//************************************************************************
// REST OF THE CONSTRUCTOR
//************************************************************************ 

}

//************************************************************************
// DECLARATIONS OF PUBLIC METHODS
//************************************************************************

LogView.prototype.setUnit = function(unit) {
    this.unit = unit;
    this.graphInstance.updateOptions({
        ylabel: this.unit
    });
};

/*
 * @author Juha Loukkola
 * Update log view. Requests data points that are newer than latest local.
 * TODO: Add/return defered object handling session timeout.
 */
LogView.prototype.update = function() {
    // TODO: Check if start is inclusive on back end.
    // Resolve the last data point.
    var from;
    if (this.log.length > 0) {
        from = (Math.round(new Date(this.log[this.log.length - 1][0]).getTime() / 1000)) + 1;
    } else {
        from = this.from;
    }
    // Get log.
    this.getLog(from, this.to, false);
};

/*
 * @author Juha Loukkola
 * Refreshes all local data points from the source.
 * TODO: Add/return defered object handling session timeout.
 */
LogView.prototype.refresh = function(includeMissing) {
    // Delete local data 
    this.log = [];
    // Get log
    this.getLog(this.from, this.to, includeMissing);
};


/*
 * @author Juha Loukkola
 */
LogView.prototype.createGraph = function() {
    this.graphInstance = new Dygraph(this.container,
            this.log,
            {
                axes: {
                    y: {
                        valueFormatter: this.getFormater(1, true),
                        pixelsPerLabel: 50
                                //axisLabelFormatter: this.getFormater(1, false)
                    },
                    x: {
                        pixelsPerLabel: 80
                    }
                },
                labelsKMG2: true,
                ylabel: this.unit,
                labels: [this.xLabel, this.yLabel],
                fillGraph: true,
                //dateWindow: [from, to],
                //rollPeriod: 50,
                stepPlot: true,
                labelsDivWidth: 400,
                labelsDivStyles: {
                    //'width': '350px',
                    //'position': 'top right',
                    'font-size': '0.9em'
                },
                includeZero: true,
                colors: ["#00bfff", "#284785"]
            });
};

/**
 * @author Juha Loukkola
 */
LogView.prototype.getLog = function(from, to, includeMissing) {
    $.ajax({
        context: this,
        type: "POST",
        url: this.dataSourceURL,
        data: {
            stream: this.stream,
            from: from,
            to: to,
            logLevel: this.resolution,
            format: "JSON",
            includeMissing: includeMissing
        }
    }).done(function(response) {
        for (x in response["points"]) {
            var x_time = new Date(response["points"][x][0]);
            var x_point = Number(response["points"][x][1]);
            this.log.push([x_time, x_point]);
        }
        this.unit = response["unit"];
        if (this.graphInstance === null) {
            // Instantiate the graph
            this.createGraph();
        } else {
            // Update the gaph
            this.graphInstance.updateOptions({
                'file': this.log,
                ylabel: this.unit
            });
        }
    }).fail(function(jqXHR, textStatus, errorThrown) {
        if (jqXHR.status === 401) {
//            alert("error " + jqXHR.status + ": \n\n" + jqXHR.responseText);
            window.location.href = "/AsemoWebUI/";
        }
    });
};

/**
 * @author Juha Loukkola
 * Based on dyGraph examble: http://dygraphs.com/tests/labelsKMB.html.
 * Creates a function that formats a numeric value into following string format: [value] [magnitude suffix][unit].
 * E.g. formatValue(-1234, 'W') returns '-1.23 kW'.
 * 
 * @return a formater function.
 */
LogView.prototype.getFormater = function(decimals, showUnit) {
    var formater = function(value, opts, dygraph) {
        // Name magnitude suffixes
        var suffixes = ['n', '\u00B5', 'm', '', 'k', 'M', 'G', 'T'];
        var suffixOffset = 3;
        var magnitude;

        // Calculate magnitude
        if (value === 0) {
            magnitude = 0;
        } else {
            magnitude = Math.floor((Math.log(Math.abs(value)) / Math.log(1000)));
        }

        // Enforce lower and upper bouds of magnitude
        if ((magnitude + suffixOffset) < 0) {
            magnitude = -suffixOffset;
        }
        else if ((magnitude + suffixOffset) >= suffixes.length) {
            magnitude = suffixes.length - 1 - suffixOffset;
        }

        // Format value
        var formatedValue = (value / Math.pow(10, magnitude * 3)).toFixed(decimals) + ' ' + suffixes[magnitude + suffixOffset];
        return showUnit ? formatedValue + this.unit : formatedValue;
    };
    return formater.bind(this);
};

/**
 * @author Juha Loukkola
 * Calculates suitale resolution for an visualization of a time series where
 * visualization should contain "maxPoints" at maximum between dates "from" and "to".
 * 
 * @param maxPoints a maximum number of values within log view
 */
LogView.prototype.adjustResolution = function(maxPoints) {
    var deltaT = (this.to - this.from);
    this.resolution = Math.round(deltaT / maxPoints) * 1000;
};

/**
 * @author Juha Loukkola
 * Resizes the underlying graph instance
 */
LogView.prototype.resize = function() {
    if (this.graphInstance !== null) {
        this.graphInstance.resize();
    }
};

LogView.prototype.roll = function() {
    if (this.log.length > 0) {
        var isInView = function(element, index, array) {
            return ((element[0] >= new Date(this.from * 1000)) && (element[0] <= new Date(this.to * 1000)));
        };
        var tmp = this.log.filter(isInView, this);
        this.log = tmp;
    }
};
