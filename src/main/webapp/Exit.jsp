<%@ include file="/taglibs.jsp" %>

<stripes:layout-render name="/layout/standard.jsp" title="Thank You For Using WattBot">
    <stripes:layout-component name="contents">
        <p>Thank you for taking the time to check out WattBot. If
        you'd like to login again, just click on any of the navigation links.</p>
    </stripes:layout-component>
</stripes:layout-render>
