<%-- 
    Document   : admin
    Created on : Jan 19, 2012, 12:54:16 PM
    Author     : juha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<stripes:layout-definition>
    <stripes:layout-render name="/layout/kvaliiti/standard.jsp">
        <stripes:layout-component name="header">
            <jsp:include page="/layout/kvaliiti/admin_header.jsp"/>
        </stripes:layout-component>
    </stripes:layout-render>
</stripes:layout-definition>
