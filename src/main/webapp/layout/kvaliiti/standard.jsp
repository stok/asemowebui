<%-- 
    Document   : kvaliiti1
    Created on : Dec 15, 2011, 9:30:34 AM
    Author     : juha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<stripes:layout-definition>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html>
        <head profile="http://www.w3.org/2005/10/profile">
            <link rel="icon"
                  type="image/vnd.microsoft.icon"
                  href="../favicon.ico" />
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>ASEMO - ${title}</title>
            <link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/reset.css"/>
            <link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/text.css"/>
<!--            <link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/grid.css"/>-->
            <link rel="stylesheet" type="text/css" href="${ctx}/css/content.css"/>
            <link rel="stylesheet" type="text/css" href="${ctx}/css/layout.css"/>

            <script type="text/javascript" src="${ctx}/js/asemo.js"></script>
            <stripes:layout-component name="scripts"/>
            <stripes:layout-component name="html-head"/>
            <!--Google Analytics-->
            <script type="text/javascript">
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-35962342-1']);
                _gaq.push(['_trackPageview']);
                (function() {
                    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                })();
            </script>
        </head>

        <body>
            <div id="container">
                <stripes:layout-component name="header"/>
                <stripes:layout-component name="leftpanel"/>
                <stripes:messages/>
                <div id="content">
                    <stripes:layout-component name="contents"/>
                </div>
            </div>
            <stripes:layout-component name="footer">
                <jsp:include page="/layout/kvaliiti/footer.jsp"/>
            </stripes:layout-component>
        </body>             
    </html>
</stripes:layout-definition>
