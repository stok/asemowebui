<%-- 
    Document   : headers
    Created on : Dec 15, 2011, 9:51:17 AM
    Author     : juha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>
<jsp:useBean id="urls" class="fi.stok.asemo.web.ui.Urls" type="fi.stok.asemo.web.ui.Urls" scope="page"/>

<div id="user_header" class="grid_full">
    <div>
        <a class="logo" href="/"><strong id="logo_text">asemo.fi</strong></a>
    </div><br/>
    <div id="user_navigation_menu" class="navigation_menu"> 
        <ul>
            <li><stripes:link beanclass="${urls.USER_GRAPH_ACTION}" title="Hetkellisseuranta">Hetkellisseuranta</stripes:link></li>
            <li><stripes:link beanclass="${urls.USER_HISTORY_ACTION}" title="Historia">Historia</stripes:link></li>
            <li><stripes:link beanclass="${urls.USER_SETTINGS_ACTION}" title="Käyttäjätiedot">Käyttäjätiedot</stripes:link></li>
            <%--<li><stripes:link href="${urls.COMPARISON_GRAPH}" title="Vertaile">Vertaile</stripes:link></li>--%>
            <%--<li><stripes:link beanclass="${urls.USER_LOG_GRAPH_ACTION}" title="Loki">Loki</stripes:link></li>--%>
            <%--<li><stripes:link href="${urls.BLOG}" title="Blogi">Blogi</stripes:link></li> --%>
            <li><stripes:link beanclass="${urls.USER_USER_LOGOUT_ACTION}" title="Kirjaudu ulos">Kirjaudu ulos</stripes:link></li>
        </ul>			
    </div>
</div>  
