<%-- 
    Document   : user
    Created on : Jan 19, 2012, 2:24:46 PM
    Author     : juha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<stripes:layout-definition>
    <stripes:layout-render name="/layout/kvaliiti/standard.jsp">
        <stripes:layout-component name="header">
            <jsp:include page="/layout/kvaliiti/user_header.jsp"/>
        </stripes:layout-component>
    </stripes:layout-render>
</stripes:layout-definition>

