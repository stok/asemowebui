<%-- 
    Document   : footer
    Created on : Dec 15, 2011, 10:36:54 AM
    Author     : juha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<%--<div id="site-info"> --%>
<div id="footer">
    <div>
        <a href="http://www.posintra.fi/stok/living-labs/"><img alt="Sähköisen talotekniikan osaamis- ja kehittämiskeskus" src="${ctx}/img/stok-small.png"></a>
        <a href="http://www.posintra.fi/"><img alt="Posintra Oy" src="${ctx}/img/posintra-small.png"></a>
        <a href="http://www.posintra.fi/stok/stok-energy-living-lab-rekisteriseloste/">Rekisteriseloste</a>
    </div><br />
</div>
<%--</div>--%>
