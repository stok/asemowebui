<%-- 
    Document   : admin_header
    Created on : Jan 19, 2012, 2:27:36 PM
    Author     : juha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>
<%--<jsp:useBean id="urls" class="fi.stok.asemo.web.ui.Urls" type="fi.stok.asemo.web.ui.Urls" scope="page"/>--%>

<div id="admin_header" class="grid_full">
    <div class="h1">
        <a class="admin_logo" href="${ctx}/admin"><strong id="logo_text">asemo.fi</strong></a>
    </div><br/>
    <div id="admin_navigation_menu" class="navigation_menu"> 
        <ul>
            <li><stripes:link beanclass="fi.stok.asemo.web.ui.stripes.admin.CustomersActionBean" title="Asiakkaat">Asiakkaat</stripes:link></li>
            <li><stripes:link beanclass="fi.stok.asemo.web.ui.stripes.admin.UsersActionBean" title="Käyttäjätiedot">Käyttäjät</stripes:link></li>
            <li><stripes:link beanclass="fi.stok.asemo.web.ui.stripes.admin.NodesActionBean" title="Seurantalaitteet">Seurantalaitteet</stripes:link></li>
            <li><stripes:link beanclass="fi.stok.asemo.web.ui.stripes.admin.InstrumentsActionBean" title="Instrumentit">Instrumentit</stripes:link></li>
            <li><stripes:link beanclass="fi.stok.asemo.web.ui.stripes.admin.InstrumentTypesActionBean" title="Instumenttityypit">Instumenttityypit</stripes:link></li>
            <li><stripes:link beanclass="fi.stok.asemo.web.ui.stripes.admin.DataSchemasActionBean" title="Skeemat">Skeemat</stripes:link></li>
            <li><stripes:link beanclass="fi.stok.asemo.web.ui.stripes.admin.LogoutActionBean" title="Kirjaudu ulos">Kirjaudu ulos</stripes:link></li>
        </ul>			
    </div>
</div>  
