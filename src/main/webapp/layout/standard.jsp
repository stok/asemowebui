<%-- 
    Document   : kvaliiti1
    Created on : Dec 15, 2011, 9:30:34 AM
    Author     : juha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<stripes:layout-definition>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html>
        <head profile="http://www.w3.org/2005/10/profile">
            <link rel="icon"
                  type="image/vnd.microsoft.icon"
                  href="../favicon.ico" />
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>ASEMO - ${title}</title>
            <link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/reset.css"/>
            <link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/text.css"/>
            <!--<link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/grid.css"/>-->

            <!-- <script type="text/javascript" src="${ctx}/scripts/asemo.js"></script> -->
            <stripes:layout-component name="scripts"/>
            <stripes:layout-component name="html-head"/>
        </head>

        <body>
            <div id="container">
                <stripes:layout-component name="header"/>
                <stripes:layout-component name="leftpanel"/>
                <stripes:messages/>
                <stripes:layout-component name="contents"/>           
                <stripes:layout-component name="footer">
                    <%--<jsp:include page="/layout/kvaliiti/footer.jsp"/>--%>
                </stripes:layout-component>
            </div>
        </body>             
    </html>
</stripes:layout-definition>
