<%-- 
    Document   : History
    Created on : Mar 7, 2012, 11:11:25 AM
    Author     : Juha Loukkola
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>
<stripes:layout-render name="/layout/kvaliiti/user.jsp" title="Historia">
    <stripes:layout-component name="scripts">
        <script type="text/javascript" src="${ctx}/js/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="${ctx}/js/jquery/jquery-migrate-1.2.1.js"></script>
        <!--[if IE]>
        <script type="text/javascript" src="utils/dygraph/excanvas.js"></script>
        <![endif]-->       
        <script type="text/javascript" src="${ctx}/js/dygraph/dygraph-combined.js"></script>

<!--        <script type="text/javascript" src="${ctx}/js/dygraph/strftime-min.js"></script>
        <script type="text/javascript" src="${ctx}/js/dygraph/rgbcolor.js"></script>
        <script type="text/javascript" src="${ctx}/js/dygraph/dygraph-canvas.js"></script>
        <script type="text/javascript" src="${ctx}/js/dygraph/dygraph.js"></script>-->

        <script type="text/javascript" src="${ctx}/js/jquery-ui/jquery-ui.js"></script>
        <script type="text/javascript" src="${ctx}/js/jquery-ui/jquery.ui.datepicker-fi.js"></script>

        <%--
        <script type="text/javascript" src="${ctx}/js/jquery-ui/jquery-ui-1.8.18.custom.min.js"></script>
        <script type="text/javascript" src="${ctx}/js/jquery-ui/jquery.ui.datepicker-fi.js"></script>
        <script type="text/javascript" src="${ctx}/js/jQuery-UI-Date-Range-Picker/daterangepicker.jQuery.js"></script>
        <script type="text/javascript" src="${ctx}/js/jQuery-UI-Date-Range-Picker/date.js"></script>
        --%>

        <script type="text/javascript" src="${ctx}/js/logView.js"></script>
        <%-- TODO: add a new layout-component "styles" and put css in that --%>
        <link rel="stylesheet" href="${ctx}/css/jquery-ui/ui-lightness/jquery-ui-1.8.18.custom.css" type="text/css">
        <link rel="stylesheet" href="${ctx}/css/jQuery-UI-Date-Range-Picker/ui.daterangepicker.css" type="text/css">
    </stripes:layout-component>
    <stripes:layout-component name="contents">
        <div id="graph_container">
            <c:choose>
                <c:when test="${empty streams}">
                    <h1>Historia: Käyttäjällä ei ole yhtään streamiä!</h1>
                </c:when>
                <c:otherwise>  
                    <h1>Historia: 
                        <c:choose>
                            <c:when test="${fn:length(streams) == 1}">
                                ${streams[0].name}
                                <input id="variable" type="hidden" value="${streams[0].id}"/> 
                            </c:when>
                            <c:otherwise>
                                <select id="variable">
                                    <c:forEach var="stream" items="${streams}" varStatus="rowCounter">
                                        <c:choose>
                                            <c:when test = "${rowCounter.count == 1}">
                                                <option value="${stream.id}" selected="selected">${stream.name}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${stream.id}">${stream.name}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </c:otherwise>
                        </c:choose>
                    </h1>
                    <div id="history_controls" class="grid_content_left">
                        <h2>Hae viimeisin</h2>
                        <ul>
                            <li><button id="fetch_day" type="button">vuorokausi</button></li>
                            <li><button id="fetch_week" type="button">7 päivää</button></li>
                            <li><button id="fetch_month" type="button">30 päivää</button></li>
                            <li><button id="fetch_year" type="button">vuosi</button></li>
                        </ul>
                        <h2>Vapaa haku</h2>
                        <dl>          
                            <dt>Alkamis pvm:</dt>
                            <dd><input id="start_date" type="text"></dd>

                            <dt>Loppumis pvm:</dt>
                            <dd><input id="end_date" type="text"></dd>
                        </dl>
                        <!--                <input type="submit" id="fetch" value="Hae">-->
                        <button id="fetch">Hae</button>

                        <script type="text/javascript">
                            // TODO: add input validation
                            var today = new Date();
                            $.datepicker.setDefaults($.datepicker.regional[ "fi" ]);
                            $('#start_date').datepicker({
                                onSelect: function(dateText, inst) {
                                    $('#end_date').datepicker("option", "minDate", new Date($("#start_date").datepicker("getDate")));

                                },
                                maxDate: today
                            });
                            //$('#start_date').datepicker( "setDate" , today);
                            $('#end_date').datepicker({
                                onSelect: function(dateText, inst) {
                                    $('#start_date').datepicker("option", "maxDate", new Date($("#end_date").datepicker("getDate")));
                                },
                                maxDate: today
                            });
                            //$('#end_date').datepicker( "setDate" , today);

                            $("#fetch").click(function() {
                                from = Math.round(new Date($("#start_date").datepicker("getDate")).getTime() / 1000);
                                // The range end should point at the end of that day
                                to = Math.round(new Date($("#end_date").datepicker("getDate")).getTime() / 1000 + 60 * 60 * 24);
                                if (from < to) {
                                    refreshGraph();
                                } else {
                                    // TODO: display validation errors
                                }
                            });
                            $("#fetch_day").click(function() {
                                to = Math.round(new Date().getTime() / 1000);
                                from = to - 60 * 60 * 24;
                                refreshGraph();
                            });
                            $("#fetch_week").click(function() {
                                to = Math.round(new Date().getTime() / 1000);
                                from = to - 60 * 60 * 24 * 7;
                                refreshGraph();
                            });
                            $("#fetch_month").click(function() {
                                to = Math.round(new Date().getTime() / 1000);
                                from = to - 60 * 60 * 24 * 30;
                                refreshGraph();
                            });
                            $("#fetch_year").click(function() {
                                to = Math.round(new Date().getTime() / 1000);
                                from = to - 60 * 60 * 24 * 365;
                                refreshGraph();
                            });

                            $('#variable').change(function() {
                                setStream($("#variable").val());
                                refreshGraph();
                            });

                        </script>
                        <br/>
                    </div>

                    <div id="history_graph">
                        <div id="graphdiv2"></div>
                        <script type="text/javascript">
                            //<!CDATA[[

                            function resize() {
                                var height = ($(window).height()) - 220;
                                width = (Math.min($(window).width(), $("#graph_container").width())) - 200;
                                if (height < min_height) {
                                    height = min_height;
                                }
                                $("div#graphdiv2").attr('style', 'width: ' + width + 'px; height: ' + height + 'px;');
                                if (lv) {
                                    lv.resize();
                                }
                            }

                            function initLogView() {
                                min_height = 80;
                                width = (Math.min($(window).width(), $("#graph_container").width())) - 200;
                                var now = new Date();

                                var container = document.getElementById("graphdiv2");
                                var stream = $("#variable").val();
                                var maxPoints = width / 1.5;
                                to = Math.round(now.getTime() / 1000);
                                from = to - 60 * 60 * 24;
                                var url = "LogData";
                                var xLabel = "Aika";
                                var yLabel = "Arvo";
                                var unit = "";
                                lv = new LogView(container, stream, maxPoints, from, to, url, xLabel, yLabel, unit);
//                                setInterval(updateGraph, 5000);
                                resize();
                            }

//                            function refreshGraph() {
//                                lv.from = from;
//                                lv.to = to;
//                                lv.update();
//                            }

                            function setStream(stream) {
                                lv.stream = stream;
                            }

                            function refreshGraph() {
                                lv.from = from;
                                lv.to = to;
                                lv.adjustResolution(width / 1.5);
                                lv.refresh(true);
                            }

                            $(window).ready(initLogView);
                            $(window).resize(resize);
                            //]]>
                        </script>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </stripes:layout-component>
</stripes:layout-render>
