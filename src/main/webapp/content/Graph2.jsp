<%-- 
    Document   : Graph
    Created on : Mar 5, 2012, 3:49:25 PM
    Author     : Juha Loukkola
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>
<stripes:layout-render name="/layout/kvaliiti/user.jsp" title="Aloitussivu">
    <stripes:layout-component name="scripts">
        <script type="text/javascript" src="${ctx}/js/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="${ctx}/js/jquery/jquery-migrate-1.2.1.js"></script>
        <!--[if IE]>
        <script type="text/javascript" src="${ctx}/js/dygraph/excanvas.js"></script>
        <![endif]-->
        <script type="text/javascript" src="${ctx}/js/dygraph/dygraph-combined.js"></script>
<!--        <script type="text/javascript" src="${ctx}/js/dygraph/strftime-min.js"></script>
        <script type="text/javascript" src="${ctx}/js/dygraph/rgbcolor.js"></script>
        <script type="text/javascript" src="${ctx}/js/dygraph/dygraph-canvas.js"></script>
        <script type="text/javascript" src="${ctx}/js/dygraph/dygraph.js"></script>-->

        <script type="text/javascript" src="${ctx}/js/logView.js"></script>
    </stripes:layout-component>
    <stripes:layout-component name="contents">
        <div id="graph_container">
            <c:choose>
                <c:when test="${empty streams}">
                    <h1>Tämänhetkinen tilanne: Käyttäjällä ei ole yhtään streamiä!</h1>
                </c:when>
                <c:otherwise>
                    <h1>Tämänhetkinen tilanne:
                        <c:choose>
                            <c:when test="${fn:length(streams) == 1}">
                                ${streams[0].name}
                                <input id="variable" type="hidden" value="${streams[0].id}"/> 
                            </c:when>
                            <c:otherwise>
                                <select id="variable">
                                    <c:forEach var="stream" items="${streams}" varStatus="rowCounter">
                                        <c:choose>
                                            <c:when test = "${rowCounter.count == 1}">
                                                <option value="${stream.id}" selected="selected">${stream.name}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${stream.id}">${stream.name}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </c:otherwise>
                        </c:choose>
                    </h1>
                    <div id="graphdiv"></div>

                    <script type="text/javascript">
                        //<!CDATA[[

                        $('#variable').change(function() {
                            changeStream($("#variable").val());
                        });

                        function resize() {
                            height = ($(window).height()) - 220;
                            if (height < min_height) {
                                height = min_height;
                            }
                            $("div#graphdiv").attr('style', 'width: 100%; height: ' + height + 'px;');
                            if (lv) {
                                lv.resize();
                            }
                        }

                        function initLogView() {
                            min_height = 80;
//                            width = (Math.min($(window).width(), $("#graph_container").width()))- 200;
                            var now = new Date();

                            var container = document.getElementById("graphdiv");
                            var stream = $("#variable").val();
                            var maxPoints = $(window).width() / 1.5;
                            to = Math.round(now.getTime() / 1000);
                            from = to - (3600 * ${!(empty hrs) ? hrs : 2});
                            var url = "LogData";
                            var xLabel = "Aika";
                            var yLabel = "Arvo";
                            var unit = "";
                            lv = new LogView(container, stream, maxPoints, from, to, url, xLabel, yLabel, unit);
                            lv.resolution = 1;
                            resize();
                            updateGraph();
                            updateInterval = setInterval(updateGraph, 5000);
                        }

                        function changeStream(stream) {
                            clearInterval(updateInterval);
                            lv.stream = stream;
                            lv.refresh(false);
                            updateInterval = setInterval(updateGraph, 5000);
                        }

                        function updateGraph() {
                            var now = new Date();
                            lv.to = Math.round(now.getTime() / 1000);
                            lv.from = lv.to - (3600 * ${!(empty hrs) ? hrs : 2});
                            lv.roll();
                            lv.update();
                        }

                        var updateInterval;
                        $(window).ready(initLogView);
                        $(window).resize(resize);

                        window.onerror = function(desc, page, line, chr)
                        {
                            alert("Error: " + desc + " @" + page + ':' + line + ':' + chr);
                        };
                        //]]>
                    </script>
                </c:otherwise>
            </c:choose>
        </div>
    </stripes:layout-component>
</stripes:layout-render>