<%-- 
    Document   : SessionExpired
    Created on : Apr 23, 2012, 9:47:38 AM
    Author     : juha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>
<jsp:useBean id="urls" class="fi.stok.asemo.web.ui.Urls" type="fi.stok.asemo.web.ui.Urls" scope="page"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>ASEMO - ${title}</title>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/reset.css"/>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/text.css"/>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/grid.css"/>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/content.css"/>
    </head>

    <body class="fp">
        <div id="container">
            <div id="header_fp">
                <div class="h1">
                    <a href="/"><strong>asemo.fi</strong></a>
                </div><br />
                <div id="navigation"> 
                </div>
            </div> 
        </div>

        <div id="site-info">
            <div id="footer">
                <div id="footer_content">
                    <ul>
                        <li><stripes:errors beanclass="${urls.USER_LOGIN_ACTION}"/></li>
                        <li><a href="#" target="_self"><span style="color:#009edf;">&raquo;</span>&nbsp;Unohditko salasanasi?</a></li>
                        <!--  <li><a href="#" target="_self"><span style="color:#009edf;">&raquo;</span>&nbsp;lyhyt kuvaus järjestelmästä (yksi lause) och samma på svenska</a></li> -->
                    </ul>
                </div>
            </div>
        </div>

    </body>
</html>
