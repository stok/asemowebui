<%-- 
    Document   : RequestNewPassword
    Created on : May 2, 2012, 2:22:04 PM
    Author     : juha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>
<jsp:useBean id="urls" class="fi.stok.asemo.web.ui.Urls" type="fi.stok.asemo.web.ui.Urls" scope="page"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>ASEMO - ${title}</title>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/reset.css"/>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/text.css"/>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/grid.css"/>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/content.css"/>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/layout.css"/>
    </head>

    <body class="fp">
        <div id="container">
            <div id="frontpage_header">
                <div id="frontpage_logo">
                    <a href="/">
                        <img alt="asemo.fi" src="img/asemo_logo_fp.png">
                    </a>
                </div>
            </div> 

            <div id="content">
                <div id="site-info">
                    <stripes:form beanclass="${urls.USER_REQUEST_PASSWORD_ACTION}" focus="">
                        <ul>
                            <li>Syötä käyttäjätunnuksesi ja paina lähetä. Saat uuden salasanan sähköpostiisi.</li>
                            <li>Käyttäjätunnus: <stripes:text name="username" size="12" value="${user.username}" class="text_input_blue"/><stripes:submit name="send" value="Lähetä" class="button"/></li>
                            <li><stripes:errors beanclass="${urls.USER_REQUEST_PASSWORD_ACTION}"/></li>
                        </ul>
                    </stripes:form>
                </div>
            </div>

        </div>

        <div id="footer">
            <div>
                <a href="http://www.posintra.fi/stok/living-labs/">
                    <img alt="Sähköisen talotekniikan osaamis- ja kehittämiskeskus" src="img/stok-small.png">
                </a>
                <a href="http://www.posintra.fi/">
                    <img alt="Posintra Oy" src="img/posintra-small.png">
                </a>
            </div>
        </div>

    </body>
</html>
