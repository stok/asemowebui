<%-- 
    Document   : ChangePassword
    Created on : Apr 27, 2012, 2:49:27 PM
    Author     : juha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<stripes:layout-render name="/layout/kvaliiti/user.jsp" title="Vaihda salasana">
    <stripes:layout-component name="contents">
        <div class="content_main" id="container">
            <h1>Vaihda salasana</h1>
            <stripes:form beanclass="fi.stok.asemo.web.ui.stripes.ChangePasswordActionBean" focus="">
                <stripes:errors/>
                <table class="table_wide">
                    <stripes:hidden name="id" value="${actionBean.id}"/>
                    <tr>
                        <td>Salasana:</td>
                        <td><stripes:password name="password" /></td>
                    </tr>
                    <tr>
                        <td>Vahvista salasana:</td>
                        <td><stripes:password name="confirmPassword" /></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <stripes:submit name="save" value="Tallenna"/>                    
                        </td>
                    </tr>
                </table>
            </stripes:form>
        </div>
    </stripes:layout-component>
</stripes:layout-render>
