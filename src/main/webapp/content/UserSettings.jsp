<%-- 
    Document   : UserSettings
    Created on : Nov 30, 2011, 2:42:20 PM
    Author     : juha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<stripes:layout-render name="/layout/kvaliiti/user.jsp" title="Käyttäjätiedot">
    <stripes:layout-component name="scripts">
        <script type="text/javascript" src="${ctx}/js/jquery/jquery.min.js"></script>
        <script>
            $(window).ready(function() {
                //$("select").selectmenu();
            });
        </script>
    </stripes:layout-component>

    <stripes:layout-component name="contents">
        <jsp:useBean id="urls" scope="page" class="fi.stok.asemo.web.ui.Urls" />
        <div id="user_info">
            <stripes:link beanclass="${urls.USER_CHANGE_INFO_ACTION}">
                <h1>Käyttäjätiedot ( ${user.username} )</h1>
                <stripes:param name="id" value="${user.id}"/>
            </stripes:link>
            <table border="0" cellpadding="0" cellspacing="0" id="choose_time">                    
                <tr><td>${customer.name}</td></tr>
                <tr><td>${customer.streetAddress}</td></tr>
                <tr><td>${customer.zipCode} ${user.customer.city}</td></tr>
                <tr><td>${customer.phone}</td></tr>
                <tr><td>${customer.email}</td></tr>
            </table>
            <stripes:link beanclass="${urls.USER_CHANGE_PASSWORD_ACTION}">
                vaihda salasana
                <stripes:param name="id" value="${user.id}"/>
            </stripes:link>
        </div>

        <div id="residence_form">
            <div>
                <stripes:form beanclass="fi.stok.asemo.web.ui.stripes.ResidenceSettingsActionBean" focus="">
                    <stripes:errors/>
                    <h1>Asunnon tiedot</h1>
                    <table class="table_loose_vertical">
                        <tr>
                            <td>
                                <stripes:select name="buildingType" value="${residence.buildingType}">
                                    <stripes:option value="">[ talotyyppi ]</stripes:option>
                                    <stripes:options-map map="${buildingTypes}"/>
                                </stripes:select>
                            </td>
                        </tr>
                        <tr>
                            <td>Asunnossa asuvien henkilöiden lukumäärä <stripes:text name="numResidents" size="2" value="${residence.numResidents}" class="text_input_blue"/></td>
                        </tr>
                        <tr>
                            <td>Bruttoala (brm2) <stripes:text name="grossArea" size="4" value="${residence.grossArea}" class="text_input_blue"/>
                                ja rakennustilavuus <stripes:text name="buildingVolume" size="4" value="${residence.buildingVolume}" class="text_input_blue"/></td>
                        </tr>

                        <tr>
                            <td>Energiatodistuksen mukainen energiankulutus (kWh/brm2/vuosi) <stripes:text name="certifiedEnergyConsumption" size="4" value="${residence.certifiedEnergyConsumption}" class="text_input_blue"/></td>
                        </tr>

                        <tr>
                            <td>Rakennusvuosi <stripes:text name="yearOfCompletion" size="4" value="${residence.yearOfCompletion}" class="text_input_blue"/>
                                ja peruskorjausvuosi <stripes:text name="yearOfrenovation" size="4" value="${residence.yearOfrenovation}" class="text_input_blue"/></td>
                        </tr>
                        <tr>
                            <td>
                                <stripes:select name="heatingMethod" value="${residence.heatingMethod}">
                                    <stripes:option value="">[ päälämmitysmuoto ]</stripes:option>
                                    <stripes:options-map map="${heatingMethods}"/>
                                </stripes:select>
                            </td>
                        </tr>
                    </table>
                    <h2>Lisäenergian tuotantotavat</h2>
                    <table>
                        <%--
                        <tr>
                            <td><stripes:checkbox name="heatStoringFireplace" value="${residence.heatStoringFireplace}"/> varaava takka</td>
                        </tr>
                        <tr>
                            <td><stripes:checkbox name="solarHeatCollector" value="${residence.solarHeatCollector}"/> aurinkokerääjä</td>
                        </tr>
                        <tr>
                            <td><stripes:checkbox name="solarPanel" value="${residence.solarPanel}"/> aurinkopaneeli</td>
                        </tr>
                        <tr>
                            <td><stripes:checkbox name="windMill" value="${residence.windMill}"/> tuulivoimala</td>
                        </tr>   
                        <tr>
                            <td><stripes:checkbox name="airHeatPump" value="${residence.airHeatPump}"/> ilmalämpöpumppu</td>
                        </tr>
                        <tr>
                            <td><stripes:checkbox name="airWaterHeatPump" value="${residence.airWaterHeatPump}"/> ilma-vesilämpöpumppu</td>
                        </tr>
                        <tr>
                            <td><stripes:checkbox name="outgoingAirHeatPump" value="${residence.outgoingAirHeatPump}"/> poistoilman lämpöpumppu</td>
                        </tr>
                        <tr>
                            <td><stripes:checkbox name="battery" value="${residence.battery}"/> akku</td>
                        </tr>
                        <tr>
                            <td><stripes:checkbox name="electricCar" value="${residence.electricCar}"/> sähkoauto</td>
                        </tr>
                        --%>
                        <tr>
                            <td><stripes:checkbox name="heatStoringFireplace"/> varaava takka</td>
                        </tr>
                        <tr>
                            <td><stripes:checkbox name="solarHeatCollector" /> aurinkokerääjä</td>
                        </tr>
                        <tr>
                            <td><stripes:checkbox name="solarPanel" /> aurinkopaneeli</td>
                        </tr>
                        <tr>
                            <td><stripes:checkbox name="windMill" /> tuulivoimala</td>
                        </tr>   
                        <tr>
                            <td><stripes:checkbox name="airHeatPump" /> ilmalämpöpumppu</td>
                        </tr>
                        <tr>
                            <td><stripes:checkbox name="airWaterHeatPump" /> ilma-vesilämpöpumppu</td>
                        </tr>
                        <tr>
                            <td><stripes:checkbox name="outgoingAirHeatPump" /> poistoilman lämpöpumppu</td>
                        </tr>
                        <tr>
                            <td><stripes:checkbox name="battery" /> akku</td>
                        </tr>
                        <tr>
                            <td><stripes:checkbox name="electricCar" /> sähköauto</td>
                        </tr>
                        <%-- TODO: refactor the code to use loop. Solve issue of unable to iterate over energySupplyTypes
                                                <c:forEach items="${energySupplyTypes}" var="energySupplyType" varStatus="loop">
                                                    <tr>
                                                        <td><stripes:checkbox name="additionalEnergySupplies" /> test</td>
                                                    </tr>
                                                </c:forEach>
                        --%>
                        <%--
                                                <tr>
                                                    <td><stripes:checkbox name="additionalEnergySupplies" value="${heatStoringFireplace}"/> varaava takka</td>
                                                </tr>
                                                <tr>
                                                    <td><stripes:checkbox name="additionalEnergySupplies" value="${solarHeatCollector}"/> aurinkokerääjä</td>
                                                </tr>
                                                <tr>
                                                    <td><stripes:checkbox name="additionalEnergySupplies" value="${solarPanel}"/> aurinkopaneeli</td>
                                                </tr>
                                                <tr>
                                                    <td><stripes:checkbox name="additionalEnergySupplies" value="${windMill}"/> tuulivoimala</td>
                                                </tr>   
                                                <tr>
                                                    <td><stripes:checkbox name="additionalEnergySupplies" value="${airHeatPump}"/> ilmalämpöpumppu</td>
                                                </tr>
                                                <tr>
                                                    <td><stripes:checkbox name="additionalEnergySupplies" value="${airWaterHeatPump}"/> ilma-vesilämpöpumppu</td>
                                                </tr>
                                                <tr>
                                                    <td><stripes:checkbox name="additionalEnergySupplies" value="${outgoingAirHeatPump}"/> poistoilman lämpöpumppu</td>
                                                </tr>
                                                <tr>
                                                    <td><stripes:checkbox name="additionalEnergySupplies" value="${battery}"/> akku</td>
                                                </tr>
                                                <tr>
                                                    <td><stripes:checkbox name="additionalEnergySupplies" value="${electricCar}"/> sähkoauto</td>
                                                </tr>
                        --%>
                        <tr>
                            <td>joku muu<stripes:text name="otherAdditionalEnergySupply" value="${residence.otherAdditionalEnergySupply}" size="30" class="text_input_blue"/></td>
                        </tr>

                    </table>
                    <h2>Lisätietoja</h2>
                    <table class="table_loose_vertical">
                        <tr>
                            <td>Tehdyt muutokset (esim. energiatehokkuutta parantavat toimenpiteet)</td>
                        </tr>
                        <tr>
                            <td>
                                <stripes:textarea name="conversionWork" value="${residence.conversionWork}" rows="10" cols="30" class="text_input_blue"/>
                                <%--value="${residence.muutokset}" --%>
                        </tr>
                        <tr>
                            <td>Pääasiallinen eristysmateriaali <stripes:text name="mainInsulationMaterial" value="${residence.mainInsulationMaterial}" size="30" class="text_input_blue"/></td>
                        </tr>

                        <tr>
                            <td>Lämmöntalteenottokoneen merkki ja malli <stripes:text name="heatRecoveryMachine" value="${residence.heatRecoveryMachine}" size="30" class="text_input_blue"/></td>
                        </tr>
                        <tr>
                            <td>Lisätietoja</td>
                        </tr>
                        <tr>
                            <td>
                                <stripes:textarea name="additionalInfo" rows="10" cols="30" value="${residence.additionalInfo}" class="text_input_blue"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <%--
                                <c:choose>            
                                    <c:when test="${user.username == 'test1'}">
                                        <stripes:submit name="save" value="Tallenna" class="button" disabled="disabled"/>
                                    </c:when>
                                    <c:otherwise>
                                --%>
                                <stripes:submit name="save" value="Tallenna" class="button"/>
                                <%--    
                                    </c:otherwise>
                                </c:choose>
                                --%>
                            </td>
                        </tr>
                    </table>
                </stripes:form>
            </div>
        </div>
        <div class="clear"></div>
    </stripes:layout-component>
</stripes:layout-render>
