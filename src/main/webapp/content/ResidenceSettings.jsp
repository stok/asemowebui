<%-- 
    Document   : index
    Created on : Nov 7, 2011, 10:15:01 AM
    Author     : juha
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="stripes" uri="http://stripes.sourceforge.net/stripes.tld"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head><title>Residence Settings</title></head>
    <body>
        <h1>Residence Settings</h1>

        <stripes:form beanclass="fi.stok.asemo.web.ui.stripes.ResidenceSettingsActionBean" focus="">
            <stripes:errors/>
            <table>
                <tr>
                    <td>Talotyyppi:</td>
                    <td><stripes:text name="type"/></td>
                </tr>
                <tr>
                    <td>Asukkaiden lukumäärä:</td>
                    <td><stripes:text name="numResidents"/></td>
                </tr>
                <tr>
                    <td>Bruttoala(brm2):</td>
                    <td><stripes:text name="grossArea"/></td>
                </tr>
                <tr>
                    <td>Rakennustilavuus:</td>
                    <td><stripes:text name="buildingVolume"/></td>
                </tr>
                <tr>
                    <td>Energiatodistuksen mukainen energiankulutus(kWh/brm2/vuosi):</td>
                    <td><stripes:text name="certifiedEnergyConsumption"/></td>
                </tr>
                <tr>
                    <td>Rakennusvuosi:</td>
                    <td><stripes:text name="yearOfCompletion"/></td>
                </tr>
                <tr>
                    <td>Peruskorjausvuosi</td>
                    <td><stripes:text name="yearOfrenovation"/></td>
                </tr>
                <tr>
                    <td>Lämmitysmuoto:</td>
                    <td><stripes:text name="heatingMethod"/></td>
                </tr>
                <tr>
                    <td>Eristys:</td>
                    <td><stripes:text name="insulation"/></td>
                </tr>
                <tr>
                    <td>Lämmön talteenotto</td>
                    <td><stripes:text name="heatRecovery"/></td>
                </tr>
                <tr>
                    <td>Lämpöpumppu</td>
                    <td><stripes:text name="heatPump"/></td>
                </tr>
                                <tr>
                    <td>Tehdyt muutostyöt</td>
                    <td><stripes:text name="alterationWork"/></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <stripes:submit name="handle" value="Save"/>                    
                    </td>
                </tr>
            </table>
        </stripes:form>
    </body>
</html>