<%-- 
    Document   : ChangeUserInfo
    Created on : Apr 27, 2012, 4:54:38 PM
    Author     : juha
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<stripes:layout-render name="/layout/kvaliiti/user.jsp" title="Muuta käytäjätietoja">
    <stripes:layout-component name="contents">
        <jsp:useBean id="urls" scope="page" class="fi.stok.asemo.web.ui.Urls" />
        <div class="content_main" id="container">
            <h1>Muuta käytäjätietoja</h1>
            <stripes:form beanclass="fi.stok.asemo.web.ui.stripes.ChangeUserInfoActionBean" focus="">
                <stripes:errors/>
                <table class="table_wide">

                    <stripes:hidden name="id" value="${actionBean.id}"/>
                    <tr>
                        <td>Katuosoite:</td>
                        <td><stripes:text name="streetAddress" value="${actionBean.customer.streetAddress}"/></td>
                    </tr>
                    <tr>
                        <td>Postinumero:</td>
                        <td><stripes:text name="zipCode" value="${actionBean.customer.zipCode}"/></td>
                    </tr>
                    <tr>
                        <td>Kaupunki:</td>
                        <td><stripes:text name="city" value="${actionBean.customer.city}"/></td>
                    </tr>
                    <tr>
                        <td>Sähköposti:</td>
                        <td><stripes:text name="email" value="${actionBean.customer.email}"/></td>
                    </tr>
                    <tr>
                        <td>Puhelinnumero:</td>
                        <td><stripes:text name="phone" value="${actionBean.customer.phone}"/></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <stripes:submit name="save" value="Tallenna"/>                    
                        </td>
                    </tr>
                </table>
            </stripes:form>
        </div>
    </stripes:layout-component>
</stripes:layout-render>
