<%-- 
    Document   : ListDataSchemaSettings
    Created on : Apr 2, 2012, 5:16:06 PM
    Author     : Juha Loukkola
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<stripes:layout-render name="/layout/kvaliiti/admin.jsp" title="Listaskeema-asetukset">

    <stripes:layout-component name="scripts">
        <script src="http://code.jquery.com/jquery-1.8.2.js"></script>
        <script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
        <script>
            function showOrHideInnerTables(selector) {
                if ($(selector).is(':hidden')) {
                    $(selector).show();
                }
                else {
                    $(selector).hide();
                }
            }

//            $(function() {
//                $( ".selectable" ).selectable();
//            });
//            
////            $(function() {
////                $( document ).tooltip();
////            });
//            
//            $(function() {
//                $( "#accordion" )
//                .accordion({
//                    header: "> div > ul "
//                });
//                //                .sortable({
//                //                    axis: "y",
//                //                    handle: "ul",
//                //                    stop: function( event, ui ) {
//                //                        // IE doesn't register the blur when sorting
//                //                        // so trigger focusout handlers to remove .ui-state-focus
//                //                        ui.item.children( "ul" ).triggerHandler( "focusout" );
//                //                    }
//                //                });
//            });
//            
//            jQuery(document).ready(function($) {
//                $('#accordion input[type="checkbox"]').click(function(e) {
//                    e.stopPropagation();
//                });
//            });

            var schemaElement = ${actionBean.elements};



        </script>
    </stripes:layout-component>

    <stripes:layout-component name="contents">
        <h1>Listaskeema-asetukset</h1>
        <stripes:form beanclass="fi.stok.asemo.web.ui.stripes.admin.ListDataSchemaSettingsActionBean" focus="">
            <stripes:errors/>
            <table class="table_wide">
                <stripes:hidden name="schema.id" value="${actionBean.schema.id}"/>
                <tr>
                    <td>Nimi:</td>
                    <td><stripes:text name="schema.name"/></td>
                </tr>
                <tr>
                    <td>Kuvaus:</td>
                    <td><stripes:textarea name="schema.description"/></td>
                </tr>
            </table>

            <div class="area">
                <h2>Seemaelementit</h2>
                <table id="schema_variables">
                    <tr>
                        <th>Valitse</th>
                        <th>Nimi</th>
                        <th>Kuvaus</th>
                        <th>Mittayksikkö</th>
                        <th>Tietotyyppi</th>
                        <th>Piilotettu</th>
                        <th>Tallenna arvot</th>
                    </tr>

                    <c:forEach items="${actionBean.elements}" var="element" varStatus="loop">
                        <stripes:hidden name="elements[${loop.index}].id"/>
                        <stripes:hidden name="elements[${loop.index}].valueGroupSettings"/>
                        <tr>
                            <td><stripes:checkbox name="deleteIds" value="${loop.index}" onclick="handleCheckboxRangeSelection(this, event)" title="Select this variable."/></td>
                            <td><stripes:text name="elements[${loop.index}].name" title="Name of the variable"/></td>
                            <td><stripes:text name="elements[${loop.index}].description" title="Short description."/></td>
                            <td><stripes:text name="elements[${loop.index}].unitOfMeasure" title="Unit of measure"/></td>
                            <td>
                                <stripes:select name="elements[${loop.index}].dataType" title="Data type.">
                                    <stripes:option value="">[ datatype ]</stripes:option>
                                    <stripes:options-collection collection="${actionBean.dataTypes}" />
                                </stripes:select>
                            </td>
                            <td><stripes:checkbox name="elements[${loop.index}].hidden" title="Is variable to be hidden?"/></td>
                            <td><stripes:checkbox name="elements[${loop.index}].store" title="Is values of the variable to be stored?"/></td>
                            <!--<td><button onclick="showOrHideInnerTables('#innerTable${loop.index}');return false;">More...</button></td>-->
                        </tr>

                        <%--
                        <tr id="innerTable${loop.index}" hidden="true" >
                            <td colspan="7">
                                <div class="inner_table">
                                    <h2>Normalization Settings</h2>
                                    <table>
                                        <tr>
                                            <th>enabled</th>
                                            <th>sampling period (ms)</th>
                                            <th>rotation period (ms)</th>
                                        </tr>
                                        <c:forEach items="${element.valueGroupSettings}" var="valueGroupSetting" varStatus="loop2">
                                            <tr>
                                                <stripes:hidden name="elements[${loop.index}].valueGroupSettings[${loop2.index}].id" />
                                                <stripes:hidden name="elements[${loop.index}].valueGroupSettings[${loop2.index}].variableType" />
                                                <!-- TODO: Is this enough to main tain relations? -->
                                                <stripes:hidden name="elements[${loop.index}].valueGroupSettings[${loop2.index}].valueGroups" /> 
                                                <td><stripes:checkbox name="elements[${loop.index}].valueGroupSettings[${loop2.index}].enabled" /></td>
                                                <td><stripes:hidden name="elements[${loop.index}].valueGroupSettings[${loop2.index}].samplingPeriod" />${valueGroupSetting.samplingPeriod}</td>
                                                <td><stripes:text name="elements[${loop.index}].valueGroupSettings[${loop2.index}].rotationPeriod" /></td>
                                            </tr>
                                        </c:forEach>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        --%>
                    </c:forEach>

                    <tr>
                        <td><stripes:submit name="addElement" value="Add" title="Add a new variable to the schema."/></td>
                        <td><stripes:text id="sf" name="newElement.name" value="[ Name ]" title="Name of the variable"/></td>
                        <td><stripes:text name="newElement.description" value="[ Description ]" size="50" title="Short description."/></td>
                        <td><stripes:text name="newElement.unitOfMeasure" value="[ Unit ]" size="10" title="Unit of measure"/></td>
                        <td>
                            <stripes:select name="newElement.dataType" title="Data type.">
                                <stripes:option value="">[ datatype ]</stripes:option>
                                <stripes:options-collection collection="${actionBean.dataTypes}" />
                            </stripes:select>
                        </td>
                        <td><stripes:checkbox name="newElement.hidden" title="Is variable to be hidden?"/></td>
                        <td><stripes:checkbox name="newElement.store" title="Is values of the variable to be stored?"/></td>
                    </tr>

                </table>
            </div>

            <%--            <div class="area">
                <h2>Schema Elements</h2> 
                <div id="schema_variables_header" class="schema_variables">
                    <div>Select</div>
                    <div>Name</div>
                    <div>Description</div>
                    <div>Unit of measure</div>
                    <div>Datatype</div>
                    <div>Hidden</div>
                    <div>Store</div>
                </div>
                <div id="accordion">
                    <c:forEach items="${actionBean.elements}" var="element" varStatus="loop">
                        <stripes:hidden name="elements[${loop.index}].id" />
                        <div class="group">      
                            <ul class="schema_variables">
                                <li><stripes:checkbox name="deleteIds" value="${loop.index}" onclick="handleCheckboxRangeSelection(this, event)" title="Select this variable."/></li>
                                <li><stripes:text name="elements[${loop.index}].name" title="Name of the variable"/></li>
                                <li><stripes:text name="elements[${loop.index}].description" title="Short description."/></li>
                                <li><stripes:text name="elements[${loop.index}].unitOfMeasure" title="Unit of measure"/></li>
                                <li>
                                    <stripes:select name="elements[${loop.index}].dataType" title="Data type.">
                                        <stripes:option value="">[ datatype ]</stripes:option>
                                        <stripes:options-collection collection="${actionBean.dataTypes}" />
                                    </stripes:select>
                                </li>
                                <li><stripes:checkbox name="elements[${loop.index}].hidden" title="Is variable to be hidden?"/></li>
                                <li><stripes:checkbox name="elements[${loop.index}].store" title="Is values of the variable to be stored?"/></li>
                            </ul>
                            <div>
                                <h2> Normalization Settings</h2>
                                <table id="variable_normalization_settings">
                                    <tr>
                                        <th>enabled</th>
                                        <th>sampling period (ms)</th>
                                        <th>rotation period (ms)</th>
                                    </tr>
                                    <c:forEach items="${element.valueGroupSettings}" var="valueGroupSetting" varStatus="loop2">
                                        <tr>
                                            <stripes:hidden name="elements[${loop.index}].valueGroupSettings[${loop2.index}].id" />
                                            <td><stripes:checkbox name="elements[${loop.index}].valueGroupSettings[${loop2.index}].enabled" /></td>
                                            <td><stripes:hidden name="elements[${loop.index}].valueGroupSettings[${loop2.index}].samplingPeriod" />${valueGroupSetting.samplingPeriod}</td>
                                            <td><stripes:text name="elements[${loop.index}].valueGroupSettings[${loop2.index}].rotationPeriod" /></td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                        </div>
                    </c:forEach>
                </div>
                <stripes:submit name="deleteElements" value="Delete selected" title="Delete selected variables."/>

                <div class="schema_variables area">
                    <ul>
                        <li><stripes:submit name="addElement" value="Add" title="Add a new variable to the schema."/></li>
                        <li><stripes:text id="sf" name="newElement.name" value="[ Name ]" title="Name of the variable"/></li>
                        <li><stripes:text name="newElement.description" value="[ Description ]" size="50" title="Short description."/></li>
                        <li><stripes:text name="newElement.unitOfMeasure" value="[ Unit ]" size="10" title="Unit of measure"/></li>
                        <li>
                            <stripes:select name="newElement.dataType" title="Data type.">
                                <stripes:option value="">[ datatype ]</stripes:option>
                                <stripes:options-collection collection="${actionBean.dataTypes}" />
                            </stripes:select>
                        </li>
                        <li><stripes:checkbox name="newElement.hidden" title="Is variable to be hidden?"/></li>
                        <li><stripes:checkbox name="newElement.store" title="Is values of the variable to be stored?"/></li>
                    </ul>
                </div>
            </div>--%>
            <c:choose>
                <c:when test="${empty actionBean.schema.id}">
                    <stripes:submit name="create" value="Tallenna"/>
                </c:when>
                <c:otherwise>
                    <stripes:submit name="save" value="Tallenna"/>
                </c:otherwise>
            </c:choose>
            <stripes:submit name="cancel" value="Peruuta"/>
        </stripes:form>
    </stripes:layout-component>

</stripes:layout-render>