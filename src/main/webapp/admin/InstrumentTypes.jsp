<%-- 
    Document   : InstrumentTypes
    Created on : Jan 30, 2012, 10:14:19 AM
    Author     : Juha Loukkola
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<stripes:layout-render name="/layout/kvaliiti/admin.jsp" title="Instrumenttityypit">
    <stripes:layout-component name="contents">

        <h1>Instrumenttityypit</h1>
        <stripes:form beanclass="fi.stok.asemo.web.ui.stripes.admin.InstrumentTypesActionBean" focus="">
            <stripes:errors/>
            <table id="customers_table">
                <tr>
                    <th>Valitse</th>
                    <th>Nimi</th>
                    <th>Kuvaus</th>
                </tr>
                <c:forEach items="${actionBean.instrumentTypes}" var="type" varStatus="loop">
                    <tr>
                        <td><stripes:checkbox name="deleteIds" value="${type.id}"
                                          onclick="handleCheckboxRangeSelection(this, event);"/></td>
                        <td>${type.name}</td>
                        <td>${type.description}</td>
                        <td>
                            <stripes:link beanclass="${urls.ADMIN_INSTRUMENT_TYPE_SETTINGS_ACTION}" event="preEdit">
                                Muokkaa
                                <stripes:param name="instrumentType.id" value="${type.id}"/>
                            </stripes:link>
                        </td>
                    </tr>
                </c:forEach>
            </table>
            <stripes:submit name="remove" value="Poista valitut"/>
            <stripes:submit name="create" value="Lisää uusi instrumenttityyppi"/>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>

