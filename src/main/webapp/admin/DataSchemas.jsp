<%-- 
    Document   : DataSchemas
    Created on : Apr 3, 2012, 11:17:21 AM
    Author     : Juha Loukkola
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<stripes:layout-render name="/layout/kvaliiti/admin.jsp" title="Skeemat">
    <stripes:layout-component name="contents">
        
            <h1>Skeemat</h1>
            <stripes:form beanclass="fi.stok.asemo.web.ui.stripes.admin.DataSchemasActionBean" focus="">
                <stripes:errors/>
                <table>
                    <tr>
                        <th>Valitse</th>
                        <th>Nimi</th>
                        <th>Kuvaus</th>
                        <th></th>
                    </tr>
                    <c:forEach items="${dataSchemas}" var="schema" varStatus="loop">
                        <tr>
                            <td><stripes:checkbox name="deleteIds" value="${schema.id}"
                                              onclick="handleCheckboxRangeSelection(this, event);"/></td>
                            <td>${schema.name}</td>
                            <td>${schema.description}</td>
                            <td>
                                <stripes:link beanclass="${urls.ADMIN_LIST_DATA_SCHEMA_SETTINGS_ACTION}" event="preEdit">
                                    Muokkaa
                                    <stripes:param name="schema.id" value="${schema.id}"/>
                                </stripes:link>
                            </td>
                        </tr>
                    </c:forEach>
                        <tr>
                            <td><stripes:submit name="delete" value="Poista valitut"/></td>
                            <td><stripes:submit name="create" value="Lisää uusi skeema"/></td>
                        </tr>
                </table>
            </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>
