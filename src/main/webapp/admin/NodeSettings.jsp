<%-- 
    Document   : NodeSettings
    Created on : Jan 19, 2012, 4:16:29 PM
    Author     : juha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<stripes:layout-render name="/layout/kvaliiti/admin.jsp" title="Node Settings">
    <stripes:layout-component name="contents">
        <h1>Node Settings</h1>
        <stripes:form beanclass="fi.stok.asemo.web.ui.stripes.admin.NodeSettingsActionBean" focus="">
            <stripes:errors/>
            <stripes:hidden name="node.id" value="${node.id}"/>
            <table>
                <tr>
                    <td>Uuid:</td>
                    <td><stripes:text name="node.uuid"/></td>
                </tr>
                <tr>
                    <td>Shared secret:</td>
                    <td><stripes:text name="node.sharedSecret"/></td>
                </tr>
                <tr>
                    <td>Hardware version:</td>
                    <td><stripes:text name="node.hardwareVersion"/></td>
                </tr>
                <tr>
                    <td>Software version:</td>
                    <td><stripes:text name="node.softwareVersion"/></td>
                </tr>
                <tr>
                    <td>Enabled:</td>
                    <td><stripes:checkbox name="node.enabled"/></td>
                </tr>
                <tr>
                    <td>Customer:</td>
                    <td>
                        <stripes:select name="customer.id" value="${actionBean.node.customer.id}">
                            <stripes:option value="">[ customer ]</stripes:option>
                            <stripes:options-collection collection="${actionBean.customers}" value="id" label="name"/>
                        </stripes:select>
                    </td>
                </tr>
            </table>
            <stripes:submit name="save" value="Save"/>
            <stripes:submit name="cancel" value="Cancel"/>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>
