<%-- 
    Document   : InstrumentTypeSettings
    Created on : Feb 16, 2012, 10:53:51 AM
    Author     : juha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<stripes:layout-render name="/layout/kvaliiti/admin.jsp" title="Instrument Type Settings">
    <stripes:layout-component name="contents">
        <h1>Instrument Type Settings</h1>
        <stripes:form beanclass="fi.stok.asemo.web.ui.stripes.admin.InstrumentTypeSettingsActionBean" focus="">
            <stripes:errors/>
            <table class="table_wide">
                <stripes:hidden name="instrumentType.id" />
                <tr>
                    <td>Name:</td>
                    <td><stripes:text name="instrumentType.name" /></td>
                </tr>
                <tr>
                    <td>Description:</td>
                    <td><stripes:textarea name="instrumentType.description" /></td>
                </tr> 
                <tr>
                    <td>Data Schema:</td>
                    <td>
                        <stripes:select name="schemaId" >
                            <stripes:option value="">[ Data Schema ]</stripes:option>
                            <stripes:options-collection collection="${actionBean.dataSchemas}" label="name" value="id"/>
                        </stripes:select>
                    </td>
                    <%--<td><stripes:textarea name="instrumentType.dataSchema" value="${actionBean.instrumentType.dataSchema}"/></td>--%>
                </tr> 
                <tr>
                    <td>Preprocessor:</td>
                    <td>
                        <stripes:select name="instrumentType.preprocessor" >
                            <stripes:option value="">[ Preprocessor ]</stripes:option>
                            <stripes:options-collection collection="${actionBean.preprocessors}" />
                        </stripes:select>
                    </td>
                    <%--<td><stripes:textarea name="instrumentType.dataSchema" value="${actionBean.instrumentType.dataSchema}"/></td>--%>
                </tr> 
            </table>
            <stripes:submit name="save" value="Save"/>
            <stripes:submit name="cancel" value="Cancel"/>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>
