<%-- 
    Document   : Instruments
    Created on : Mar 7, 2012, 3:59:21 PM
    Author     : Juha Loukkola
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<stripes:layout-render name="/layout/kvaliiti/admin.jsp" title="Instrumentit">
    <stripes:layout-component name="contents">

        <h1>Instrumentit</h1>
        <stripes:form beanclass="fi.stok.asemo.web.ui.stripes.admin.InstrumentsActionBean" focus="">
            <stripes:errors/>
            <table  class="table_wide">
                <tr>
                    <th>Valitse</th>
                    <th>UUID</th>
                    <th>Noodi(uuid)</th>
                    <th>Noodin haltija</th>
                    <th>Tyyppi</th>
                </tr>

                <c:forEach items="${actionBean.instruments}" var="instrument" varStatus="loop">
                    <tr>
                        <td><stripes:checkbox name="deleteIds" value="${instrument.id}"
                                          onclick="handleCheckboxRangeSelection(this, event);"/></td>
                        <td>${instrument.uuid}</td>
                        <td>${instrument.node.uuid}</td>
                        <td>${instrument.node.customer.name}</td>
                        <td>${instrument.dataSource.type.name}</td>
                        <td>
                            <stripes:link beanclass="${urls.ADMIN_INSTRUMENT_SETTINGS_ACTION}" event="preEdit">
                                Edit
                                <stripes:param name="instrument.id" value="${instrument.id}"/>
                            </stripes:link>
                        </td>
                    </tr>
                </c:forEach>

                <tr>
                    <td><stripes:submit name="remove" value="Poista valitut"/></td>
                    <td><stripes:submit name="create" value="Luo uusi instrumentti"/></td>
                </tr>
            </table>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>
