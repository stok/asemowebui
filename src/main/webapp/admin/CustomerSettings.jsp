<%-- 
    Document   : index
    Created on : Nov 7, 2011, 10:15:01 AM
    Author     : juha


    TODO:   option-collections need to be defined

--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<stripes:layout-render name="/layout/kvaliiti/admin.jsp" title="Customer Settings">
    <stripes:layout-component name="contents">
        <jsp:useBean id="urls" scope="page" class="fi.stok.asemo.web.ui.Urls" />
        <h1>Customer Settings</h1>

        <stripes:form beanclass="fi.stok.asemo.web.ui.stripes.admin.CustomerSettingsActionBean" focus="">

            <h2>Contact Information</h2>
            <stripes:errors/>
            <table class="table_wide">
                <stripes:hidden name="customer.id" value="${actionBean.customer.id}"/>
                <tr>
                    <td>Name:</td>
                    <td><stripes:text name="customer.name" /></td>
                </tr>
                <tr>
                    <td>Street address:</td>
                    <td><stripes:text name="customer.streetAddress" /></td>
                </tr>
                <tr>
                    <td>Zipcode:</td>
                    <td><stripes:text name="customer.zipCode" /></td>
                </tr>
                <tr>
                    <td>City:</td>
                    <td><stripes:text name="customer.city" /></td>
                </tr>
                <tr>
                    <td>E-mail address:</td>
                    <td><stripes:text name="customer.email" /></td>
                </tr>
                <tr>
                    <td>Phone number:</td>
                    <td><stripes:text name="customer.phone" /></td>
                </tr>
            </table>

            <h2>User account</h2>
            <table class="table_wide"> 
                <tr>
                    <td>
                        <stripes:select name="user.id">
                            <stripes:option value="">[ user ]</stripes:option>
                            <stripes:options-collection collection="${actionBean.availableUsers}" value="id" label="username"/>
                        </stripes:select>
                    </td>
                </tr>
            </table>

        <stripes:submit name="save" value="Save"/>
        <stripes:submit name="cancel" value="Cancel"/>
    </stripes:form>          
</stripes:layout-component>
</stripes:layout-render>