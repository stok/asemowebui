<%-- 
    Document   : Nodes
    Created on : Feb 29, 2012, 10:36:11 AM
    Author     : Juha Loukkola
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<stripes:layout-render name="/layout/kvaliiti/admin.jsp" title="Noodit">
    <stripes:layout-component name="contents">

        <h1>Noodit</h1>
        <stripes:form beanclass="fi.stok.asemo.web.ui.stripes.admin.NodesActionBean" focus="">
            <stripes:errors/>
            <table class="table_wide">
                <tr>
                    <th>Valitse</th>
                    <th>Uuid</th>
                    <th>Asiakas</th>
                    <th>Salasana</th>
                    <th>HW versio</th>
                    <th>SW versio</th>
                    <th>IP-osoite</th>
                    <th>Viimeisin yhteys</th>
                    <th>Viimeisin pyyntö</th>
                    <th>Viimeisin palvelimen vastaus</th>
                    <th>Aktivoitu</th>
                </tr>
                <c:forEach items="${actionBean.nodes}" var="node" varStatus="loop">
                    <tr>
                        <td><stripes:checkbox name="deleteIds" value="${node.id}"
                                          onclick="handleCheckboxRangeSelection(this, event);"/></td>
                        <td>${node.uuid}</td>
                        <td>${node.customer.name}</td>
                        <td>${node.sharedSecret}</td>
                        <td>${node.hardwareVersion}</td>
                        <td>${node.softwareVersion}</td>
                        <td>${node.ipAddress}</td>
                        <td>${node.lastHeard.toString()}</td>
                        <td>${node.lastRmiFunction}</td>
                        <td>${node.lastRmiResponse}</td>
                        <td>${node.enabled}</td>
                        <td>
                            <stripes:link beanclass="${urls.ADMIN_NODE_SETTINGS_ACTION}" event="preEdit">
                                Muokkaa
                                <stripes:param name="node.id" value="${node.id}"/>
                            </stripes:link>
                        </td>
                    </tr>
                </c:forEach>
                <tr>
                    <td><stripes:submit name="remove" value="Poista valitut"/></td>
                    <td><stripes:submit name="create" value="Luo uusi noodi"/></td>
                </tr>
            </table>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>

