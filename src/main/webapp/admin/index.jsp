<%-- 
    Document   : Login
    Created on : Feb 16, 2012, 9:59:46 AM
    Author     : juha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>
<jsp:useBean id="urls" class="fi.stok.asemo.web.ui.Urls" type="fi.stok.asemo.web.ui.Urls" scope="page"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head profile="http://www.w3.org/2005/10/profile">
        <link rel="icon"
              type="image/vnd.microsoft.icon"
              href="${ctx}/favicon.ico" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>ASEMO - Administrator</title>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/reset.css"/>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/text.css"/>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/kvaliiti/grid.css"/>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/content.css"/>
        <link rel="stylesheet" type="text/css" href="${ctx}/css/layout.css"/>
    </head>

    <body class="fp">
        <div id="container">

            <div id="frontpage_header">
                <div id="frontpage_logo">
                    <!--                    <a href="/">-->
                    <img alt="asemo.fi" src="${ctx}/img/asemo_admin_logo_fp.png">
                        <!--                    </a>-->
                </div>

                <div id="navigation"> 

                    <stripes:form beanclass="${urls.ADMIN_LOGIN_ACTION}" focus="">
                        <table class="fp text_default">
                            <tr>
                                <th><stripes:label for="Käyttäjätunnus"/></th>
                                <th><stripes:label for="Salasana"/></th>                    
                            </tr>
                            <tr>
                                <td><stripes:text name="username" size="12" value="${user.username}" class="text_input_blue"/></td>
                                <td><stripes:password name="password" size="12" class="text_input_blue"/></td>
                                <td style="text-align: center;">
                                    <%-- If the security servlet attached a targetUrl, carry that along. --%>
                                    <stripes:hidden name="targetUrl" value="${request.parameterMap['targetUrl']}"/>
                                    <stripes:submit name="login" value="Kirjaudu sisään" class="button"/>
                                </td>
                            </tr>
                        </table>
                    </stripes:form>
                </div>
            </div> 

            <div id="content">
                <div id="site-info">
                    <ul>
                        <li><stripes:errors beanclass="${urls.ADMIN_LOGIN_ACTION}"/></li>
                    </ul>
                </div>
            </div>    

        </div>

        <div id="footer">
            <div>
                <a href="http://www.stok.fi/">
                    <img alt="Sähköisen talotekniikan osaamis- ja kehittämiskeskus" src="${ctx}/img/stok-small.png">
                </a>
                <a href="http://www.posintra.fi/">
                    <img alt="Posintra Oy" src="${ctx}/img/posintra-small.png">
                </a>
                <a href="http://www.stok.fi/Hankkeet/STOK%20Rekisteriseloste.html">
                    Rekisteriseloste
                </a>
            </div>
        </div>

    </body>
</html>
