<%-- 
    Document   : SchemaElementSettings
    Created on : May 29, 2012, 8:31:57 PM
    Author     : juha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<stripes:layout-render name="/layout/kvaliiti/admin.jsp" title="Schema Element Settings">
    <stripes:layout-component name="contents">
        <h1>Schema Element Settings</h1>
        <stripes:form beanclass="fi.stok.asemo.web.ui.stripes.admin.SchemaElementSettingsActionBean" focus="">
            <stripes:errors/>
            <stripes:hidden name="element.id" value="${element.id}"/>
            <table>
                <tr>
                    <td>Name:</td>
                    <td><stripes:text name="element.name" value="${element.name}"/></td>
                </tr>
                <tr>
                    <td>Description:</td>
                    <td><stripes:text name="element.description" value="${element.description}"/></td>
                </tr>
                <tr>
                    <td>Unit of Measure:</td>
                    <td><stripes:text name="element.unitOfMeasure" value="${element.unitOfMeasure}"/></td>
                </tr>
                <tr>
                    <td>Data type:</td>
                    <td><stripes:text name="element.dataType" value="${element.dataType}"/></td>
                </tr>
                <tr>
                    <td>
                        <stripes:submit name="cancel" value="Cancel"/>                    
                    </td>
                    <td>
                        <stripes:submit name="save" value="Save"/>                    
                    </td>
                </tr>
            </table>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>

