<%-- 
    Document   : Users
    Created on : Nov 7, 2011, 10:15:01 AM
    Author     : Juha Loukkola
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<stripes:layout-render name="/layout/kvaliiti/admin.jsp" title="Käyttäjät">
    <stripes:layout-component name="contents">

        <h1>Käyttäjät</h1>
        <stripes:form beanclass="fi.stok.asemo.web.ui.stripes.admin.UsersActionBean" focus="">
            <stripes:errors/>      
            <table class="table_wide">
                <tr>
                    <th>Valitse</th>
                    <th>Käyttäjänimi</th>
                    <th>Nimi</th>
                    <th>Käyttäjäryhmät</th>
                </tr>
                <c:forEach items="${actionBean.users}" var="user" varStatus="loop">
                    <tr>
                        <td><stripes:checkbox name="deleteIds" value="${user.id}"
                                          onclick="handleCheckboxRangeSelection(this, event);"/></td>
                        <td>${user.username}</td>
                        <td>${user.customer.name}</td>
                        <td>
                            <c:forEach items="${user.groups}" var="group" varStatus="loop">
                                ${group.name} 
                            </c:forEach>    
                        </td>
                        <td>
                            <stripes:link beanclass="${urls.ADMIN_USER_SETTINGS_ACTION}" event="preEdit">
                                Muokkaa
                                <stripes:param name="user.id" value="${user.id}"/>
                            </stripes:link>
                        </td>
                        <c:if test="${!empty user.customer}">
                            <td>
                                <stripes:link beanclass="${urls.ADMIN_USER_MONITOR_ACTION}" target="_blank">
                                    Kirjaudu sisään käyttäjänä
                                    <stripes:param name="user.id" value="${user.id}"/>
                                </stripes:link>
                            </td>
                        </c:if>
                    </tr>
                </c:forEach>
            </table>
            <stripes:submit name="remove" value="Poista valitut"/>
            <stripes:submit name="create" value="Luo uusi käyttäjä"/>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>
