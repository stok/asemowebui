<%-- 
    Document   : Customers
    Created on : Dec 23, 2011, 3:28:02 PM
    Author     : juha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<stripes:layout-render name="/layout/kvaliiti/admin.jsp" title="Asiakkaat">
    <stripes:layout-component name="contents">

        <h1>Asiakkaat</h1>
        <stripes:form beanclass="fi.stok.asemo.web.ui.stripes.admin.CustomersActionBean" focus="">
            <stripes:errors/>
            <table id="customers_table">
                <tr>
                    <th>Valitse</th>
                    <th>Nimi</th>
                    <th>Katuosoite</th>
                    <th>Postinumero</th>
                    <th>Kaupunki</th>
                    <th>Sähköposti</th>
                    <th>Puhelin</th>
                    <th>Käyttäjänimi</th>
                    <th></th>
                </tr>
                <c:forEach items="${actionBean.customers}" var="customer" varStatus="loop">
                    <tr>
                        <td><stripes:checkbox name="deleteIds" value="${customer.id}"
                                          onclick="handleCheckboxRangeSelection(this, event);"/></td>
                        <td>${customer.name}</td>
                        <td>${customer.streetAddress}</td>
                        <td>${customer.zipCode}</td>
                        <td>${customer.city}</td>
                        <td>${customer.email}</td>
                        <td>${customer.phone}</td>
                        <td>${customer.user.username}</td>
                        <td>
                            <stripes:link beanclass="${urls.ADMIN_CUSTOMER_SETTINGS_ACTION}" event="preEdit">
                                Muokkaa
                                <stripes:param name="customer.id" value="${customer.id}"/>
                            </stripes:link>
                        </td>
                    </tr>
                </c:forEach>
            </table>
            <stripes:submit name="remove" value="Poista valitut"/>
            <stripes:submit name="create" value="Lisää uusi asiakas"/>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>
