<%-- 
    Document   : InstrumentSettings
    Created on : Mar 7, 2012, 4:09:25 PM
    Author     : Juha Loukkola
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<stripes:layout-render name="/layout/kvaliiti/admin.jsp" title="Instrumenttiasetukset">
    <stripes:layout-component name="contents">
        <h1>Instrumenttiasetukset</h1>
        <stripes:form beanclass="fi.stok.asemo.web.ui.stripes.admin.InstrumentSettingsActionBean" focus="">
            <stripes:errors/>
            <stripes:hidden name="instrument.id" value="${actionBean.instrument.id}"/>
            <table>
                <tr>
                    <td>Uuid:</td>
                    <td>
                        <c:choose>
                            <c:when test="${empty actionBean.instrument.id}"><stripes:text name="instrument.uuid"/></c:when>
                            <c:otherwise>${actionBean.instrument.uuid}</c:otherwise>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td>Tyyppi:</td>
                    <td>
                        <c:choose>
                            <c:when test="${empty actionBean.instrument.id}">
                                <stripes:select name="instrumentType.id" value="${actionBean.instrument.dataSource.type.id}">
                                    <stripes:option value="">[ tyyppi ]</stripes:option>
                                    <stripes:options-collection collection="${actionBean.instrumentTypes}" value="id" label="name"/>
                                </stripes:select>
                            </c:when>
                            <c:otherwise>${actionBean.instrument.dataSource.type.name}</c:otherwise>
                        </c:choose>
                    </td>
                </tr>

                <tr>
                    <td>Noodi:</td>
                    <td>                    
                        <stripes:select name="node.id" value="${actionBean.instrument.node.id}">
                            <stripes:option value="">[ noodi ]</stripes:option>
                            <stripes:options-collection collection="${actionBean.nodes}" value="id" label="uuid"/>
                        </stripes:select>
                    </td>
                </tr>
            </table>
            <c:choose>
                <c:when test="${empty actionBean.instrument.id}">
                    <stripes:submit name="create" value="Tallenna"/>
                </c:when>
                <c:otherwise>
                    <stripes:submit name="save" value="Tallenna"/>
                </c:otherwise>
            </c:choose>
            <stripes:submit name="cancel" value="Peruuta"/>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>

