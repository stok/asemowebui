<%-- 
    Document   : index
    Created on : Nov 7, 2011, 10:15:01 AM
    Author     : Juha Loukkola
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/taglibs.jsp" %>

<stripes:layout-render name="/layout/kvaliiti/admin.jsp" title="Käyttäjätiedot">
    <stripes:layout-component name="contents">
        <h1>Käyttäjätiedot</h1>
        
        <stripes:form beanclass="fi.stok.asemo.web.ui.stripes.admin.UserSettingsActionBean" focus="">
            <stripes:errors/>
            <stripes:hidden name="user.id" value="${actionBean.user.id}"/>

            <%--
            <h2>Asiakas</h2>
            <stripes:select name="customer.id" value="${actionBean.user.customer.id}">
                <stripes:option value="">[ asiakas ]</stripes:option>
                <stripes:options-collection collection="${actionBean.customers}" value="id" label="name"/>
            </stripes:select>
            --%>

            <h2>Tunnistautuminen</h2>
            <table class="table_wide">                  
                <tr>
                    <td>Käyttäjänimi:</td>
                    <td><stripes:text name="user.username" /></td>
                </tr>                 
                <tr>
                    <td>Salasana:</td>
                    <td><stripes:password name="password" /></td>
                </tr>
                <tr>
                    <td>Vahvista salasana:</td>
                    <td><stripes:password name="confirmPassword" /></td>
                </tr>
            </table>

            <h2>Roolit</h2>
            <table class="table_wide"> 
                <c:forEach items="${actionBean.groups}" var="group" varStatus="loop">
                    <tr>
                        <td><stripes:checkbox name="assignedGroups" value="${group.name}"/> ${group.name}</td>
                    </tr>
                </c:forEach>
            </tr>
        </table>
        <stripes:submit name="save" value="Tallenna"/>
        <stripes:submit name="cancel" value="Peruuta"/>
    </stripes:form>
</stripes:layout-component>
</stripes:layout-render>
